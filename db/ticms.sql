/*
navicat mysql data transfer

source server         : 111.230.157.133_3306_ubuntu
source server version : 50721
source host           : 111.230.157.133:3306
source database       : ticms

target server type    : mysql
target server version : 50721
file encoding         : 65001

date: 2019-01-28 10:30:38
*/

drop table if exists qrtz_fired_triggers;
drop table if exists qrtz_paused_trigger_grps;
drop table if exists qrtz_scheduler_state;
drop table if exists qrtz_locks;
drop table if exists qrtz_simple_triggers;
drop table if exists qrtz_simprop_triggers;
drop table if exists qrtz_cron_triggers;
drop table if exists qrtz_blob_triggers;
drop table if exists qrtz_triggers;
drop table if exists qrtz_job_details;
drop table if exists qrtz_calendars;


create table qrtz_job_details
(
  sched_name        varchar(120) not null,
  job_name          varchar(200) not null,
  job_group         varchar(200) not null,
  description       varchar(250) null,
  job_class_name    varchar(250) not null,
  is_durable        varchar(1)   not null,
  is_nonconcurrent  varchar(1)   not null,
  is_update_data    varchar(1)   not null,
  requests_recovery varchar(1)   not null,
  job_data          blob null,
  primary key (sched_name, job_name, job_group)
);

create table qrtz_triggers
(
  sched_name     varchar(120) not null,
  trigger_name   varchar(200) not null,
  trigger_group  varchar(200) not null,
  job_name       varchar(200) not null,
  job_group      varchar(200) not null,
  description    varchar(250) null,
  next_fire_time bigint(13) null,
  prev_fire_time bigint(13) null,
  priority       integer null,
  trigger_state  varchar(16)  not null,
  trigger_type   varchar(8)   not null,
  start_time     bigint(13) not null,
  end_time       bigint(13) null,
  calendar_name  varchar(200) null,
  misfire_instr  smallint(2) null,
  job_data       blob null,
  primary key (sched_name, trigger_name, trigger_group),
  foreign key (sched_name, job_name, job_group)
    references qrtz_job_details (sched_name, job_name, job_group)
);

create table qrtz_simple_triggers
(
  sched_name      varchar(120) not null,
  trigger_name    varchar(200) not null,
  trigger_group   varchar(200) not null,
  repeat_count    bigint(7) not null,
  repeat_interval bigint(12) not null,
  times_triggered bigint(10) not null,
  primary key (sched_name, trigger_name, trigger_group),
  foreign key (sched_name, trigger_name, trigger_group)
    references qrtz_triggers (sched_name, trigger_name, trigger_group)
);

create table qrtz_cron_triggers
(
  sched_name      varchar(120) not null,
  trigger_name    varchar(200) not null,
  trigger_group   varchar(200) not null,
  cron_expression varchar(200) not null,
  time_zone_id    varchar(80),
  primary key (sched_name, trigger_name, trigger_group),
  foreign key (sched_name, trigger_name, trigger_group)
    references qrtz_triggers (sched_name, trigger_name, trigger_group)
);

create table qrtz_simprop_triggers
(
  sched_name    varchar(120) not null,
  trigger_name  varchar(200) not null,
  trigger_group varchar(200) not null,
  str_prop_1    varchar(512) null,
  str_prop_2    varchar(512) null,
  str_prop_3    varchar(512) null,
  int_prop_1    int null,
  int_prop_2    int null,
  long_prop_1   bigint null,
  long_prop_2   bigint null,
  dec_prop_1    numeric(13,4) null,
  dec_prop_2    numeric(13,4) null,
  bool_prop_1   varchar(1) null,
  bool_prop_2   varchar(1) null,
  primary key (sched_name, trigger_name, trigger_group),
  foreign key (sched_name, trigger_name, trigger_group)
    references qrtz_triggers (sched_name, trigger_name, trigger_group)
);

create table qrtz_blob_triggers
(
  sched_name    varchar(120) not null,
  trigger_name  varchar(200) not null,
  trigger_group varchar(200) not null,
  blob_data     blob null,
  primary key (sched_name, trigger_name, trigger_group),
  foreign key (sched_name, trigger_name, trigger_group)
    references qrtz_triggers (sched_name, trigger_name, trigger_group)
);

create table qrtz_calendars
(
  sched_name    varchar(120) not null,
  calendar_name varchar(200) not null,
  calendar      blob         not null,
  primary key (sched_name, calendar_name)
);

create table qrtz_paused_trigger_grps
(
  sched_name    varchar(120) not null,
  trigger_group varchar(200) not null,
  primary key (sched_name, trigger_group)
);

create table qrtz_fired_triggers
(
  sched_name        varchar(120) not null,
  entry_id          varchar(95)  not null,
  trigger_name      varchar(200) not null,
  trigger_group     varchar(200) not null,
  instance_name     varchar(200) not null,
  fired_time        bigint(13) not null,
  sched_time        bigint(13) not null,
  priority          integer      not null,
  state             varchar(16)  not null,
  job_name          varchar(200) null,
  job_group         varchar(200) null,
  is_nonconcurrent  varchar(1) null,
  requests_recovery varchar(1) null,
  primary key (sched_name, entry_id)
);

create table qrtz_scheduler_state
(
  sched_name        varchar(120) not null,
  instance_name     varchar(200) not null,
  last_checkin_time bigint(13) not null,
  checkin_interval  bigint(13) not null,
  primary key (sched_name, instance_name)
);

create table qrtz_locks
(
  sched_name varchar(120) not null,
  lock_name  varchar(40)  not null,
  primary key (sched_name, lock_name)
);

set
foreign_key_checks
=
0;
-- ----------------------------
-- table structure for t_dept
-- ----------------------------
drop table if exists `t_dept`;
create table `t_dept`
(
  `dept_id`   bigint(20) not null auto_increment comment '部门id',
  `parent_id` bigint(20) not null comment '上级部门id',
  `dept_name` varchar(100) not null comment '部门名称',
  `order_num` double(20,
  0
) default null comment '排序',
  `create_time` datetime default null comment '创建时间',
  `modify_time` datetime default null comment '修改时间',
  primary key
(
  `dept_id`
) using btree
  ) engine=innodb auto_increment=7 default charset=utf8 row_format=dynamic;

-- ----------------------------
-- records of t_dept
-- ----------------------------
insert into `t_dept`
values ('1', '0', '开发部', '1', '2018-01-04 15:42:26', '2019-01-05 21:08:27');
insert into `t_dept`
values ('2', '1', '开发一部', '1', '2018-01-04 15:42:34', '2019-01-18 00:59:37');
insert into `t_dept`
values ('3', '1', '开发二部', '2', '2018-01-04 15:42:29', '2019-01-05 14:09:39');
insert into `t_dept`
values ('4', '0', '市场部', '2', '2018-01-04 15:42:36', '2019-01-23 06:27:56');
insert into `t_dept`
values ('5', '0', '人事部', '3', '2018-01-04 15:42:32', '2019-01-23 06:27:59');
insert into `t_dept`
values ('6', '0', '测试部', '4', '2018-01-04 15:42:38', '2019-01-17 08:15:47');

-- ----------------------------
-- table structure for t_dict
-- ----------------------------
drop table if exists `t_dict`;
create table `t_dict`
(
  `dict_id`    bigint(20) not null auto_increment comment '字典id',
  `keyy`       bigint(20) not null comment '键',
  `valuee`     varchar(100) not null comment '值',
  `field_name` varchar(100) not null comment '字段名称',
  `table_name` varchar(100) not null comment '表名',
  primary key (`dict_id`) using btree
) engine=innodb auto_increment=34 default charset=utf8 row_format=dynamic;
-- ----------------------------
-- Records of t_dict
-- ----------------------------
insert into `t_dict`
values ('1', '0', '男', 'ssex', 't_user');
insert into `t_dict`
values ('2', '1', '女', 'ssex', 't_user');
insert into `t_dict`
values ('3', '2', '保密', 'ssex', 't_user');
insert into `t_dict`
values ('4', '1', '有效', 'status', 't_user');
insert into `t_dict`
values ('5', '0', '锁定', 'status', 't_user');
insert into `t_dict`
values ('6', '0', '菜单', 'type', 't_menu');
insert into `t_dict`
values ('7', '1', '按钮', 'type', 't_menu');
insert into `t_dict`
values ('30', '0', '正常', 'status', 't_job');
insert into `t_dict`
values ('31', '1', '暂停', 'status', 't_job');
insert into `t_dict`
values ('32', '0', '成功', 'status', 't_job_log');
insert into `t_dict`
values ('33', '1', '失败', 'status', 't_job_log');

-- ----------------------------
-- table structure for t_job
-- ----------------------------
drop table if exists `t_job`;
create table `t_job`
(
  `job_id`          bigint(20) not null auto_increment comment '任务id',
  `bean_name`       varchar(100) not null comment 'spring bean名称',
  `method_name`     varchar(100) not null comment '方法名',
  `params`          varchar(200) default null comment '参数',
  `cron_expression` varchar(100) not null comment 'cron表达式',
  `status`          char(2)      not null comment '任务状态  0：正常  1：暂停',
  `remark`          varchar(200) default null comment '备注',
  `create_time`     datetime     default null comment '创建时间',
  primary key (`job_id`) using btree
) engine=innodb auto_increment=12 default charset=utf8 row_format=dynamic;

-- ----------------------------
-- records of t_job
-- ----------------------------
insert into `t_job`
values ('1', 'testtask', 'test', 'mrbird', '0/1 * * * * ?1', '1', '有参任务调度测试', '2018-02-24 16:26:14');
insert into `t_job`
values ('2', 'testtask', 'test1', null, '0/10 * * * * ?', '1', '无参任务调度测试', '2018-02-24 17:06:23');
insert into `t_job`
values ('3', 'testtask', 'test', 'hello world', '0/1 * * * * ?', '1', '有参任务调度测试,每隔一秒触发', '2018-02-26 09:28:26');
insert into `t_job`
values ('11', 'testtask', 'test2', null, '0/5 * * * * ?', '1', '测试异常', '2018-02-26 11:15:30');

-- ----------------------------
-- table structure for t_job_log
-- ----------------------------
drop table if exists `t_job_log`;
create table `t_job_log`
(
  `log_id`      bigint(20) not null auto_increment comment '任务日志id',
  `job_id`      bigint(20) not null comment '任务id',
  `bean_name`   varchar(100) not null comment 'spring bean名称',
  `method_name` varchar(100) not null comment '方法名',
  `params`      varchar(200)  default null comment '参数',
  `status`      char(2)      not null comment '任务状态    0：成功    1：失败',
  `error`       text comment '失败信息',
  `times`       decimal(11,0) default null comment '耗时(单位：毫秒)',
  `create_time` datetime      default null comment '创建时间',
  primary key (`log_id`) using btree
) engine=innodb auto_increment=2502 default charset=utf8 row_format=dynamic;
-- ----------------------------
-- Records of t_job_log
-- ----------------------------
INSERT INTO `t_job_log`
VALUES ('2450', '3', 'testTask', 'test', 'hello world', '0', null, '2', '2018-03-20 15:31:52');
INSERT INTO `t_job_log`
VALUES ('2451', '3', 'testTask', 'test', 'hello world', '0', null, '0', '2018-03-20 15:31:53');
INSERT INTO `t_job_log`
VALUES ('2452', '3', 'testTask', 'test', 'hello world', '0', null, '2', '2018-03-20 15:31:54');
INSERT INTO `t_job_log`
VALUES ('2453', '3', 'testTask', 'test', 'hello world', '0', null, '1', '2018-03-20 15:31:55');
INSERT INTO `t_job_log`
VALUES ('2454', '3', 'testTask', 'test', 'hello world', '0', null, '0', '2018-03-20 15:31:56');
INSERT INTO `t_job_log`
VALUES ('2455', '3', 'testTask', 'test', 'hello world', '0', null, '1', '2018-03-20 15:31:57');
INSERT INTO `t_job_log`
VALUES ('2456', '3', 'testTask', 'test', 'hello world', '0', null, '1', '2018-03-20 15:31:59');
INSERT INTO `t_job_log`
VALUES ('2457', '3', 'testTask', 'test', 'hello world', '0', null, '1', '2018-03-20 15:31:59');
INSERT INTO `t_job_log`
VALUES ('2458', '3', 'testTask', 'test', 'hello world', '0', null, '1', '2018-03-20 15:32:00');
INSERT INTO `t_job_log`
VALUES ('2459', '3', 'testTask', 'test', 'hello world', '0', null, '0', '2018-03-20 15:32:01');
INSERT INTO `t_job_log`
VALUES ('2460', '3', 'testTask', 'test', 'hello world', '0', null, '5', '2018-03-20 15:32:02');
INSERT INTO `t_job_log`
VALUES ('2461', '3', 'testTask', 'test', 'hello world', '0', null, '1', '2018-03-20 15:32:03');
INSERT INTO `t_job_log`
VALUES ('2462', '3', 'testTask', 'test', 'hello world', '0', null, '1', '2018-03-20 15:32:04');
INSERT INTO `t_job_log`
VALUES ('2463', '3', 'testTask', 'test', 'hello world', '0', null, '1', '2018-03-20 15:32:05');
INSERT INTO `t_job_log`
VALUES ('2464', '3', 'testTask', 'test', 'hello world', '0', null, '1', '2018-03-20 15:32:06');
INSERT INTO `t_job_log`
VALUES ('2465', '11', 'testTask', 'test2', null, '1',
        'java.lang.NoSuchMethodException: cc.mrbird.job.task.TestTask.test2()', '0', '2018-03-20 15:32:26');
INSERT INTO `t_job_log`
VALUES ('2466', '2', 'testTask', 'test1', null, '0', null, '1', '2018-04-02 15:26:40');
INSERT INTO `t_job_log`
VALUES ('2467', '2', 'testTask', 'test1', null, '0', null, '1', '2018-04-02 15:26:50');
INSERT INTO `t_job_log`
VALUES ('2468', '2', 'testTask', 'test1', null, '0', null, '1', '2018-04-02 15:27:20');
INSERT INTO `t_job_log`
VALUES ('2469', '2', 'testTask', 'test1', null, '0', null, '3', '2018-04-02 17:29:20');
INSERT INTO `t_job_log`
VALUES ('2476', '1', 'testTask', 'test', 'mrbird', '0', null, '1', '2019-01-06 08:25:00');
INSERT INTO `t_job_log`
VALUES ('2477', '11', 'testTask', 'test2', null, '1',
        'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', '0', '2019-01-06 08:25:25');
INSERT INTO `t_job_log`
VALUES ('2478', '1', 'testTask', 'test', 'mrbird', '0', null, '1', '2019-01-06 08:40:15');
INSERT INTO `t_job_log`
VALUES ('2479', '1', 'testTask', 'test', 'mrbird', '0', null, '1', '2019-01-06 08:40:15');
INSERT INTO `t_job_log`
VALUES ('2480', '1', 'testTask', 'test', 'mrbird', '0', null, '1', '2019-01-06 08:40:15');
INSERT INTO `t_job_log`
VALUES ('2481', '1', 'testTask', 'test', 'mrbird', '0', null, '1', '2019-01-06 08:40:15');
INSERT INTO `t_job_log`
VALUES ('2482', '1', 'testTask', 'test', 'mrbird', '0', null, '0', '2019-01-06 08:40:15');
INSERT INTO `t_job_log`
VALUES ('2483', '1', 'testTask', 'test', 'mrbird', '0', null, '1', '2019-01-06 08:40:15');
INSERT INTO `t_job_log`
VALUES ('2484', '1', 'testTask', 'test', 'mrbird', '0', null, '0', '2019-01-06 08:40:15');
INSERT INTO `t_job_log`
VALUES ('2485', '1', 'testTask', 'test', 'mrbird', '0', null, '0', '2019-01-06 08:40:15');
INSERT INTO `t_job_log`
VALUES ('2486', '1', 'testTask', 'test', 'mrbird', '0', null, '0', '2019-01-06 08:40:15');
INSERT INTO `t_job_log`
VALUES ('2487', '1', 'testTask', 'test', 'mrbird', '0', null, '0', '2019-01-06 08:40:15');
INSERT INTO `t_job_log`
VALUES ('2488', '1', 'testTask', 'test', 'mrbird', '0', null, '1', '2019-01-06 08:40:16');
INSERT INTO `t_job_log`
VALUES ('2489', '1', 'testTask', 'test', 'mrbird', '0', null, '0', '2019-01-06 08:40:17');
INSERT INTO `t_job_log`
VALUES ('2490', '1', 'testTask', 'test', 'mrbird', '0', null, '1', '2019-01-06 08:40:18');
INSERT INTO `t_job_log`
VALUES ('2491', '1', 'testTask', 'test', 'mrbird', '0', null, '0', '2019-01-06 08:40:19');
INSERT INTO `t_job_log`
VALUES ('2492', '1', 'testTask', 'test', 'mrbird', '0', null, '1', '2019-01-06 08:40:20');
INSERT INTO `t_job_log`
VALUES ('2493', '1', 'testTask', 'test', 'mrbird', '0', null, '0', '2019-01-06 08:40:21');
INSERT INTO `t_job_log`
VALUES ('2494', '1', 'testTask', 'test', 'mrbird', '0', null, '0', '2019-01-06 08:40:22');
INSERT INTO `t_job_log`
VALUES ('2495', '11', 'testTask', 'test2', null, '1',
        'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', '2', '2019-01-06 08:40:36');
INSERT INTO `t_job_log`
VALUES ('2496', '11', 'testTask', 'test2', null, '1',
        'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', '0', '2019-01-06 08:40:36');
INSERT INTO `t_job_log`
VALUES ('2497', '11', 'testTask', 'test2', null, '1',
        'java.lang.NoSuchMethodException: cc.mrbird.febs.job.task.TestTask.test2()', '1', '2019-01-06 08:40:40');
INSERT INTO `t_job_log`
VALUES ('2498', '2', 'testTask', 'test1', null, '0', null, '1', '2019-01-06 11:36:20');
INSERT INTO `t_job_log`
VALUES ('2499', '1', 'testTask', 'test', 'mrbird', '0', null, '30', '2019-01-22 05:41:01');
INSERT INTO `t_job_log`
VALUES ('2500', '1', 'testTask', 'test', 'mrbird', '0', null, '9', '2019-01-23 06:28:58');
INSERT INTO `t_job_log`
VALUES ('2501', '1', 'testTask', 'test', 'mrbird', '0', null, '12', '2019-01-24 05:39:59');

-- ----------------------------
-- Table structure for t_log
-- ----------------------------
drop table if exists `t_log`;
create table `t_log`
(
  `id`          bigint(20) not null auto_increment comment '日志id',
  `username`    varchar(50)   default null comment '操作用户',
  `operation`   text comment '操作内容',
  `time`        decimal(11,0) default null comment '耗时',
  `method`      text comment '操作方法',
  `params`      text comment '方法参数',
  `ip`          varchar(64)   default null comment '操作者ip',
  `create_time` datetime      default null comment '创建时间',
  `location`    varchar(50)   default null comment '操作地点',
  primary key (`id`) using btree
) engine=innodb auto_increment=1839 default charset=utf8 row_format=dynamic;
-- ----------------------------
-- Records of t_log
-- ----------------------------
INSERT INTO `t_log`
VALUES ('1815', 'mrbird', '删除用户', '301', 'cc.mrbird.febs.system.controller.UserController.deleteUsers()',
        ' userIds: \"11\"', '127.0.0.1', '2019-01-23 06:26:43', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1816', 'mrbird', '修改菜单/按钮', '170', 'cc.mrbird.febs.system.controller.MenuController.updateMenu()',
        ' menu: \"Menu(menuId=2, parentId=0, menuName=系统监控, path=/monitor, component=PageView, perms=null, icon=dashboard, type=0, orderNum=2.0, createTime=null, modifyTime=Wed Jan 23 14:27:12 CST 2019, createTimeFrom=null, createTimeTo=null)\"',
        '127.0.0.1', '2019-01-23 06:27:13', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1817', 'mrbird', '修改部门', '90', 'cc.mrbird.febs.system.controller.DeptController.updateDept()',
        ' dept: \"Dept(deptId=4, parentId=0, deptName=市场部, orderNum=2.0, createTime=null, modifyTime=Wed Jan 23 14:27:55 CST 2019, createTimeFrom=null, createTimeTo=null)\"',
        '127.0.0.1', '2019-01-23 06:27:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1818', 'mrbird', '修改部门', '596', 'cc.mrbird.febs.system.controller.DeptController.updateDept()',
        ' dept: \"Dept(deptId=5, parentId=0, deptName=人事部, orderNum=3.0, createTime=null, modifyTime=Wed Jan 23 14:27:59 CST 2019, createTimeFrom=null, createTimeTo=null)\"',
        '127.0.0.1', '2019-01-23 06:28:00', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1819', 'mrbird', '执行定时任务', '146', 'cc.mrbird.febs.job.controller.JobController.runJob()', ' jobId: \"1\"',
        '127.0.0.1', '2019-01-23 06:28:58', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1820', 'mrbird', '新增菜单/按钮', '160', 'cc.mrbird.febs.system.controller.MenuController.addMenu()',
        ' menu: \"Menu(menuId=130, parentId=3, menuName=导出Excel, path=null, component=null, perms=user:export, icon=null, type=1, orderNum=null, createTime=Wed Jan 23 14:35:15 CST 2019, modifyTime=null, createTimeFrom=null, createTimeTo=null)\"',
        '127.0.0.1', '2019-01-23 06:35:16', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1821', 'mrbird', '新增菜单/按钮', '255', 'cc.mrbird.febs.system.controller.MenuController.addMenu()',
        ' menu: \"Menu(menuId=131, parentId=4, menuName=导出Excel, path=null, component=null, perms=role:export, icon=null, type=1, orderNum=null, createTime=Wed Jan 23 14:35:36 CST 2019, modifyTime=null, createTimeFrom=null, createTimeTo=null)\"',
        '127.0.0.1', '2019-01-23 06:35:36', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1822', 'mrbird', '新增菜单/按钮', '172', 'cc.mrbird.febs.system.controller.MenuController.addMenu()',
        ' menu: \"Menu(menuId=132, parentId=5, menuName=导出Excel, path=null, component=null, perms=menu:export, icon=null, type=1, orderNum=null, createTime=Wed Jan 23 14:36:04 CST 2019, modifyTime=null, createTimeFrom=null, createTimeTo=null)\"',
        '127.0.0.1', '2019-01-23 06:36:05', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1823', 'mrbird', '新增菜单/按钮', '188', 'cc.mrbird.febs.system.controller.MenuController.addMenu()',
        ' menu: \"Menu(menuId=133, parentId=6, menuName=导出Excel, path=null, component=null, perms=dept:export, icon=null, type=1, orderNum=null, createTime=Wed Jan 23 14:36:24 CST 2019, modifyTime=null, createTimeFrom=null, createTimeTo=null)\"',
        '127.0.0.1', '2019-01-23 06:36:25', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1824', 'mrbird', '新增菜单/按钮', '186', 'cc.mrbird.febs.system.controller.MenuController.addMenu()',
        ' menu: \"Menu(menuId=134, parentId=64, menuName=导出Excel, path=null, component=null, perms=dict:export, icon=null, type=1, orderNum=null, createTime=Wed Jan 23 14:36:43 CST 2019, modifyTime=null, createTimeFrom=null, createTimeTo=null)\"',
        '127.0.0.1', '2019-01-23 06:36:44', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1825', 'mrbird', '新增菜单/按钮', '160', 'cc.mrbird.febs.system.controller.MenuController.addMenu()',
        ' menu: \"Menu(menuId=135, parentId=3, menuName=密码重置, path=null, component=null, perms=user:reset, icon=null, type=1, orderNum=null, createTime=Wed Jan 23 14:36:59 CST 2019, modifyTime=null, createTimeFrom=null, createTimeTo=null)\"',
        '127.0.0.1', '2019-01-23 06:37:00', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1826', 'mrbird', '新增菜单/按钮', '181', 'cc.mrbird.febs.system.controller.MenuController.addMenu()',
        ' menu: \"Menu(menuId=136, parentId=10, menuName=导出Excel, path=null, component=null, perms=log:export, icon=null, type=1, orderNum=null, createTime=Wed Jan 23 14:37:26 CST 2019, modifyTime=null, createTimeFrom=null, createTimeTo=null)\"',
        '127.0.0.1', '2019-01-23 06:37:27', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1827', 'mrbird', '新增菜单/按钮', '146', 'cc.mrbird.febs.system.controller.MenuController.addMenu()',
        ' menu: \"Menu(menuId=137, parentId=102, menuName=导出Excel, path=null, component=null, perms=job:export, icon=null, type=1, orderNum=null, createTime=Wed Jan 23 14:37:59 CST 2019, modifyTime=null, createTimeFrom=null, createTimeTo=null)\"',
        '127.0.0.1', '2019-01-23 06:37:59', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1828', 'mrbird', '新增菜单/按钮', '164', 'cc.mrbird.febs.system.controller.MenuController.addMenu()',
        ' menu: \"Menu(menuId=138, parentId=109, menuName=导出Excel, path=null, component=null, perms=jobLog:export, icon=null, type=1, orderNum=null, createTime=Wed Jan 23 14:38:32 CST 2019, modifyTime=null, createTimeFrom=null, createTimeTo=null)\"',
        '127.0.0.1', '2019-01-23 06:38:33', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1829', 'mrbird', '修改角色', '3132', 'cc.mrbird.febs.system.controller.RoleController.updateRole()',
        ' role: \"Role(roleId=1, roleName=管理员, remark=管理员, createTime=null, modifyTime=Wed Jan 23 14:45:28 CST 2019, createTimeFrom=null, createTimeTo=null, menuId=1,3,11,12,13,4,14,15,16,5,17,18,19,6,20,21,22,64,65,66,67,2,8,23,10,24,113,121,122,124,123,125,101,102,103,104,105,106,107,108,109,110,58,59,61,81,82,83,127,128,129,130,135,131,132,133,134,136,137,138)\"',
        '127.0.0.1', '2019-01-23 06:45:32', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1830', 'mrbird', '修改角色', '1730', 'cc.mrbird.febs.system.controller.RoleController.updateRole()',
        ' role: \"Role(roleId=2, roleName=注册用户, remark=只可查看不可操作, createTime=null, modifyTime=Wed Jan 23 15:31:07 CST 2019, createTimeFrom=null, createTimeTo=null, menuId=3,1,4,5,6,64,2,8,10,113,121,122,124,123,125,101,102,109,58,59,61,81,82,83,127,128,129)\"',
        '127.0.0.1', '2019-01-23 07:31:09', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1831', 'mrbird', '修改角色', '1997', 'cc.mrbird.febs.system.controller.RoleController.updateRole()',
        ' role: \"Role(roleId=2, roleName=注册用户, remark=可查看，新增，导出, createTime=null, modifyTime=Wed Jan 23 15:32:20 CST 2019, createTimeFrom=null, createTimeTo=null, menuId=3,1,4,5,6,64,2,8,10,113,121,122,124,123,125,101,102,109,58,59,61,81,82,83,127,128,129,130,14,17,132,20,133,65,134,136,103,137,138)\"',
        '127.0.0.1', '2019-01-23 07:32:22', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1832', 'mrbird', '新增角色', '1428', 'cc.mrbird.febs.system.controller.RoleController.addRole()',
        ' role: \"Role(roleId=72, roleName=普通用户, remark=只可查看，好可怜哦, createTime=Wed Jan 23 15:33:20 CST 2019, modifyTime=null, createTimeFrom=null, createTimeTo=null, menuId=1,3,4,5,6,64,2,8,10,113,121,122,124,123,127,101,102,109,58,59,61,81,82,83,128,129)\"',
        '127.0.0.1', '2019-01-23 07:33:22', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1833', 'mrbird', '新增用户', '338', 'cc.mrbird.febs.system.controller.UserController.addUser()',
        ' user: \"User(userId=12, username=jack, password=552649f10640385d0728a80a4242893e, deptId=6, deptName=null, email=jack@hotmail.com, mobile=null, status=1, createTime=Wed Jan 23 15:34:05 CST 2019, modifyTime=null, lastLoginTime=null, ssex=0, description=null, avatar=default.jpg, roleId=72, roleName=null, sortField=null, sortOrder=null, createTimeFrom=null, createTimeTo=null, id=null)\"',
        '127.0.0.1', '2019-01-23 07:34:06', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1834', 'mrbird', '修改角色', '2160', 'cc.mrbird.febs.system.controller.RoleController.updateRole()',
        ' role: \"Role(roleId=2, roleName=注册用户, remark=可查看，新增，导出, createTime=null, modifyTime=Wed Jan 23 15:37:08 CST 2019, createTimeFrom=null, createTimeTo=null, menuId=3,1,4,5,6,64,2,8,10,113,121,122,124,123,125,101,102,109,58,59,61,81,82,83,127,128,129,130,14,17,132,20,133,65,134,136,103,137,138,131)\"',
        '127.0.0.1', '2019-01-23 07:37:11', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1835', 'mrbird', '新增角色', '169', 'cc.mrbird.febs.system.controller.RoleController.addRole()',
        ' role: \"Role(roleId=73, roleName=测试xss, remark=<style>body{background:red !important}</style>, createTime=Wed Jan 23 15:47:04 CST 2019, modifyTime=null, createTimeFrom=null, createTimeTo=null, menuId=1,3)\"',
        '127.0.0.1', '2019-01-23 07:47:04', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log`
VALUES ('1836', 'mrbird', '删除角色', '54', 'cc.mrbird.febs.system.controller.RoleController.deleteRoles()',
        ' roleIds: \"73\"', '218.104.237.213', '2019-01-24 03:03:41', '中国|华东|福建省|福州市|联通');
INSERT INTO `t_log`
VALUES ('1837', 'mrbird', '修改用户', '39', 'cc.mrbird.febs.system.controller.UserController.updateUser()',
        ' user: \"User(userId=12, username=jack, password=null, deptId=6, deptName=null, email=jack@hotmail.com, mobile=null, status=1, createTime=null, modifyTime=Thu Jan 24 11:08:00 CST 2019, lastLoginTime=null, ssex=0, description=null, avatar=null, roleId=72, roleName=null, sortField=null, sortOrder=null, createTimeFrom=null, createTimeTo=null, id=null)\"',
        '218.104.237.213', '2019-01-24 03:08:01', '中国|华东|福建省|福州市|联通');
INSERT INTO `t_log`
VALUES ('1838', 'mrbird', '执行定时任务', '41', 'cc.mrbird.febs.job.controller.JobController.runJob()', ' jobId: \"1\"',
        '218.104.237.213', '2019-01-24 05:39:59', '中国|华东|福建省|福州市|联通');

-- ----------------------------
-- Table structure for t_login_log
-- ----------------------------
drop table if exists `t_login_log`;
create table `t_login_log`
(
  `username`   varchar(100) not null comment '用户名',
  `login_time` datetime     not null comment '登录时间',
  `location`   varchar(255) default null comment '登录地点',
  `ip`         varchar(100) default null comment 'ip地址'
) engine=innodb default charset=utf8 row_format=dynamic;
-- ----------------------------
-- Records of t_login_log
-- ----------------------------
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-12 03:18:33', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-10 03:18:33', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-09 03:18:33', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-11 03:18:33', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-12 04:23:45', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-15 03:31:18', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-15 03:36:28', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-15 06:05:36', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-15 08:44:39', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-15 09:02:42', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('scott', '2019-01-15 09:24:21', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('scott', '2019-01-15 09:25:16', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-15 10:14:20', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('scott', '2019-01-15 10:48:59', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-14 11:02:04', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('scott', '2019-01-13 11:02:04', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-15 11:02:04', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-16 01:20:24', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-16 02:25:47', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-16 03:25:11', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-16 03:44:23', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-16 05:44:05', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-16 05:51:12', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('scott', '2019-01-16 05:51:21', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-16 05:54:03', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-16 06:18:57', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-16 06:31:19', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-16 07:32:02', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 01:10:42', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 02:21:12', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 06:07:00', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 06:45:24', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 06:46:40', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 06:54:23', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 06:54:53', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 06:55:38', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 07:38:37', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 07:39:14', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 07:40:48', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 07:41:41', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('scott', '2019-01-17 07:42:53', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('scott', '2019-01-17 07:43:39', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 08:13:29', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 08:39:56', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 09:26:19', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 09:26:58', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 09:30:15', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbirdd', '2019-01-17 10:31:40', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('jack', '2019-01-17 10:41:14', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('abcd', '2019-01-17 10:47:48', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('abcd', '2019-01-17 10:48:06', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('abcd', '2019-01-17 10:48:44', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('abcd', '2019-01-17 10:51:35', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('wuyouzhugu', '2019-01-17 10:54:56', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 10:56:53', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 10:59:15', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 10:59:53', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 11:01:54', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 11:08:43', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 11:12:55', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-17 11:13:21', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-18 00:56:15', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-18 01:21:54', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-18 01:33:06', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-18 02:03:32', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-18 02:27:12', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-18 02:36:26', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-18 02:41:49', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-18 02:53:12', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-18 02:56:00', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-18 03:00:35', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-18 05:36:02', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-18 05:57:39', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-18 06:50:27', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-18 07:09:37', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-18 08:57:02', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-18 09:00:06', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-19 01:13:17', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-19 01:14:42', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-19 01:50:38', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-19 02:05:44', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-19 02:06:52', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-19 02:11:47', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-19 02:12:13', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-19 02:12:27', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-19 02:33:21', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-19 02:40:19', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-21 03:05:20', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-21 03:16:03', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-21 05:43:32', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-21 05:44:20', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-21 06:47:04', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-21 06:49:51', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-21 07:48:30', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-21 07:50:34', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-21 07:55:22', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-21 07:57:39', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-21 08:35:07', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-21 08:58:37', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-21 11:05:26', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-22 00:47:44', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-22 01:02:23', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-22 01:38:19', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-22 02:39:18', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-22 05:39:47', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-22 05:44:25', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-22 06:04:18', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-22 06:04:34', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-22 06:13:00', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-22 06:13:17', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('scott', '2019-01-22 06:13:43', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('scott', '2019-01-22 06:14:41', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('scott', '2019-01-22 06:15:10', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('hello', '2019-01-22 06:15:48', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('hello', '2019-01-22 06:17:19', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('hello', '2019-01-22 06:18:39', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-22 06:19:03', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-22 06:20:48', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-22 07:04:26', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-22 07:06:07', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-22 07:06:57', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-22 08:37:28', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-22 10:29:50', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-23 00:50:47', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-23 01:51:42', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-23 02:58:49', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-23 06:11:14', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('scott', '2019-01-23 06:46:30', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-23 06:48:25', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('scott', '2019-01-23 06:51:20', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-23 07:30:25', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('scott', '2019-01-23 07:34:28', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('jack', '2019-01-23 07:35:56', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-23 07:36:46', '内网IP|0|0|内网IP|内网IP', '127.0.0.1');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-24 01:30:13', '中国|华东|福建省|福州市|联通', '218.104.237.213');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-24 01:42:03', '中国|华东|福建省|福州市|联通', '218.104.237.213');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-24 01:48:10', '中国|华东|福建省|福州市|联通', '218.104.237.213');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-24 01:50:12', '中国|华东|福建省|福州市|联通', '218.104.237.213');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-24 01:50:28', '中国|华东|福建省|福州市|联通', '218.104.237.213');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-24 02:57:48', '中国|华东|福建省|福州市|联通', '218.104.237.213');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-24 03:02:53', '中国|华东|福建省|福州市|联通', '218.104.237.213');
INSERT INTO `t_login_log`
VALUES ('scott', '2019-01-24 03:14:51', '中国|华东|福建省|厦门市|电信', '120.36.172.239');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-24 03:41:10', '中国|华东|福建省|福州市|联通', '218.104.237.213');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-24 05:38:30', '中国|华东|福建省|福州市|联通', '218.104.237.213');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-24 08:50:44', '中国|华东|福建省|福州市|联通', '218.104.237.213');
INSERT INTO `t_login_log`
VALUES ('jack', '2019-01-24 08:52:03', '中国|华东|福建省|福州市|联通', '218.104.237.213');
INSERT INTO `t_login_log`
VALUES ('scott', '2019-01-24 08:52:31', '中国|华东|福建省|福州市|联通', '218.104.237.213');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-24 11:12:33', '中国|华东|福建省|福州市|联通', '218.104.237.213');
INSERT INTO `t_login_log`
VALUES ('scott', '2019-01-24 11:24:04', '中国|华东|福建省|福州市|联通', '218.104.237.213');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-24 11:47:56', '中国|华东|福建省|福州市|电信', '27.155.195.27');
INSERT INTO `t_login_log`
VALUES ('scott', '2019-01-24 11:48:28', '中国|华东|福建省|福州市|电信', '27.155.195.27');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-28 01:53:06', '中国|华东|福建省|福州市|联通', '218.104.237.213');
INSERT INTO `t_login_log`
VALUES ('mrbird', '2019-01-28 01:53:58', '中国|华东|福建省|福州市|联通', '218.104.237.213');
INSERT INTO `t_login_log`
VALUES ('scott', '2019-01-28 01:54:09', '中国|华东|福建省|福州市|联通', '218.104.237.213');

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
drop table if exists `t_menu`;
create table `t_menu`
(
  `menu_id`   bigint(20) not null auto_increment comment '菜单/按钮id',
  `parent_id` bigint(20) not null comment '上级菜单id',
  `menu_name` varchar(50) not null comment '菜单/按钮名称',
  `path`      varchar(255) default null comment '对应路由path',
  `component` varchar(255) default null comment '对应路由组件component',
  `perms`     varchar(50)  default null comment '权限标识',
  `icon`      varchar(50)  default null comment '图标',
  `type`      char(2)     not null comment '类型 0菜单 1按钮',
  `order_num` double(20,
  0
) default null comment '排序',
  `create_time` datetime not null comment '创建时间',
  `modify_time` datetime default null comment '修改时间',
  primary key
(
  `menu_id`
) using btree
  ) engine=innodb auto_increment=139 default charset=utf8 row_format=dynamic;
-- ----------------------------
-- Records of t_menu
-- ----------------------------
INSERT INTO `t_menu`
VALUES ('1', '0', '系统管理', '/system', 'PageView', null, 'appstore-o', '0', '1', '2017-12-27 16:39:07',
        '2019-01-05 11:13:14');
INSERT INTO `t_menu`
VALUES ('2', '0', '系统监控', '/monitor', 'PageView', null, 'dashboard', '0', '2', '2017-12-27 16:45:51',
        '2019-01-23 06:27:12');
INSERT INTO `t_menu`
VALUES ('3', '1', '用户管理', '/system/user', 'system/user/User', 'user:view', '', '0', '1', '2017-12-27 16:47:13',
        '2019-01-22 06:45:55');
INSERT INTO `t_menu`
VALUES ('4', '1', '角色管理', '/system/role', 'system/role/Role', 'role:view', '', '0', '2', '2017-12-27 16:48:09',
        '2018-04-25 09:01:12');
INSERT INTO `t_menu`
VALUES ('5', '1', '菜单管理', '/system/menu', 'system/menu/Menu', 'menu:view', '', '0', '3', '2017-12-27 16:48:57',
        '2018-04-25 09:01:30');
INSERT INTO `t_menu`
VALUES ('6', '1', '部门管理', '/system/dept', 'system/dept/Dept', 'dept:view', '', '0', '4', '2017-12-27 16:57:33',
        '2018-04-25 09:01:40');
INSERT INTO `t_menu`
VALUES ('8', '2', '在线用户', '/monitor/online', 'monitor/Online', 'user:online', '', '0', '1', '2017-12-27 16:59:33',
        '2018-04-25 09:02:04');
INSERT INTO `t_menu`
VALUES ('10', '2', '系统日志', '/monitor/systemlog', 'monitor/SystemLog', 'log:view', '', '0', '2', '2017-12-27 17:00:50',
        '2018-04-25 09:02:18');
INSERT INTO `t_menu`
VALUES ('11', '3', '新增用户', '', '', 'user:add', null, '1', null, '2017-12-27 17:02:58', null);
INSERT INTO `t_menu`
VALUES ('12', '3', '修改用户', '', '', 'user:update', null, '1', null, '2017-12-27 17:04:07', null);
INSERT INTO `t_menu`
VALUES ('13', '3', '删除用户', '', '', 'user:delete', null, '1', null, '2017-12-27 17:04:58', null);
INSERT INTO `t_menu`
VALUES ('14', '4', '新增角色', '', '', 'role:add', null, '1', null, '2017-12-27 17:06:38', null);
INSERT INTO `t_menu`
VALUES ('15', '4', '修改角色', '', '', 'role:update', null, '1', null, '2017-12-27 17:06:38', null);
INSERT INTO `t_menu`
VALUES ('16', '4', '删除角色', '', '', 'role:delete', null, '1', null, '2017-12-27 17:06:38', null);
INSERT INTO `t_menu`
VALUES ('17', '5', '新增菜单', '', '', 'menu:add', null, '1', null, '2017-12-27 17:08:02', null);
INSERT INTO `t_menu`
VALUES ('18', '5', '修改菜单', '', '', 'menu:update', null, '1', null, '2017-12-27 17:08:02', null);
INSERT INTO `t_menu`
VALUES ('19', '5', '删除菜单', '', '', 'menu:delete', null, '1', null, '2017-12-27 17:08:02', null);
INSERT INTO `t_menu`
VALUES ('20', '6', '新增部门', '', '', 'dept:add', null, '1', null, '2017-12-27 17:09:24', null);
INSERT INTO `t_menu`
VALUES ('21', '6', '修改部门', '', '', 'dept:update', null, '1', null, '2017-12-27 17:09:24', null);
INSERT INTO `t_menu`
VALUES ('22', '6', '删除部门', '', '', 'dept:delete', null, '1', null, '2017-12-27 17:09:24', null);
INSERT INTO `t_menu`
VALUES ('23', '8', '踢出用户', '', '', 'user:kickout', null, '1', null, '2017-12-27 17:11:13', null);
INSERT INTO `t_menu`
VALUES ('24', '10', '删除日志', '', '', 'log:delete', null, '1', null, '2017-12-27 17:11:45', null);
INSERT INTO `t_menu`
VALUES ('58', '0', '网络资源', '/web', 'PageView', null, 'compass', '0', '4', '2018-01-12 15:28:48', '2018-01-22 19:49:26');
INSERT INTO `t_menu`
VALUES ('59', '58', '天气查询', '/web/weather', 'web/Weather', 'weather:view', '', '0', '1', '2018-01-12 15:40:02',
        '2019-01-22 05:43:19');
INSERT INTO `t_menu`
VALUES ('61', '58', '每日一文', '/web/dailyArticle', 'web/DailyArticle', 'article:view', '', '0', '2',
        '2018-01-15 17:17:14', '2019-01-22 05:43:27');
INSERT INTO `t_menu`
VALUES ('64', '1', '字典管理', '/system/dict', 'system/dict/Dict', 'dict:view', '', '0', '5', '2018-01-18 10:38:25',
        '2018-04-25 09:01:50');
INSERT INTO `t_menu`
VALUES ('65', '64', '新增字典', '', '', 'dict:add', null, '1', null, '2018-01-18 19:10:08', null);
INSERT INTO `t_menu`
VALUES ('66', '64', '修改字典', '', '', 'dict:update', null, '1', null, '2018-01-18 19:10:27', null);
INSERT INTO `t_menu`
VALUES ('67', '64', '删除字典', '', '', 'dict:delete', null, '1', null, '2018-01-18 19:10:47', null);
INSERT INTO `t_menu`
VALUES ('81', '58', '影视资讯', '/web/movie', 'EmptyPageView', null, null, '0', '3', '2018-01-22 14:12:59',
        '2019-01-22 05:43:35');
INSERT INTO `t_menu`
VALUES ('82', '81', '正在热映', '/web/movie/hot', 'web/MovieHot', 'movie:hot', '', '0', '1', '2018-01-22 14:13:47',
        '2019-01-22 05:43:52');
INSERT INTO `t_menu`
VALUES ('83', '81', '即将上映', '/web/movie/coming', 'web/MovieComing', 'movie:coming', '', '0', '2', '2018-01-22 14:14:36',
        '2019-01-22 05:43:58');
INSERT INTO `t_menu`
VALUES ('101', '0', '任务调度', '/job', 'PageView', null, 'clock-circle-o', '0', '3', '2018-01-11 15:52:57', null);
INSERT INTO `t_menu`
VALUES ('102', '101', '定时任务', '/job/job', 'quartz/job/Job', 'job:view', '', '0', '1', '2018-02-24 15:53:53',
        '2019-01-22 05:42:50');
INSERT INTO `t_menu`
VALUES ('103', '102', '新增任务', '', '', 'job:add', null, '1', null, '2018-02-24 15:55:10', null);
INSERT INTO `t_menu`
VALUES ('104', '102', '修改任务', '', '', 'job:update', null, '1', null, '2018-02-24 15:55:53', null);
INSERT INTO `t_menu`
VALUES ('105', '102', '删除任务', '', '', 'job:delete', null, '1', null, '2018-02-24 15:56:18', null);
INSERT INTO `t_menu`
VALUES ('106', '102', '暂停任务', '', '', 'job:pause', null, '1', null, '2018-02-24 15:57:08', null);
INSERT INTO `t_menu`
VALUES ('107', '102', '恢复任务', '', '', 'job:resume', null, '1', null, '2018-02-24 15:58:21', null);
INSERT INTO `t_menu`
VALUES ('108', '102', '立即执行任务', '', '', 'job:run', null, '1', null, '2018-02-24 15:59:45', null);
INSERT INTO `t_menu`
VALUES ('109', '101', '调度日志', '/job/log', 'quartz/log/JobLog', 'jobLog:view', '', '0', '2', '2018-02-24 16:00:45',
        '2019-01-22 05:42:59');
INSERT INTO `t_menu`
VALUES ('110', '109', '删除日志', '', '', 'jobLog:delete', null, '1', null, '2018-02-24 16:01:21', null);
INSERT INTO `t_menu`
VALUES ('113', '2', 'Redis监控', '/monitor/redis/info', 'monitor/RedisInfo', 'redis:view', '', '0', '3',
        '2018-06-28 14:29:42', null);
INSERT INTO `t_menu`
VALUES ('121', '2', '请求追踪', '/monitor/httptrace', 'monitor/Httptrace', null, null, '0', '4', '2019-01-18 02:30:29',
        null);
INSERT INTO `t_menu`
VALUES ('122', '2', '系统信息', '/monitor/system', 'EmptyPageView', null, null, '0', '5', '2019-01-18 02:31:48',
        '2019-01-18 02:39:46');
INSERT INTO `t_menu`
VALUES ('123', '122', 'Tomcat信息', '/monitor/system/tomcatinfo', 'monitor/TomcatInfo', null, null, '0', '2',
        '2019-01-18 02:32:53', '2019-01-18 02:46:57');
INSERT INTO `t_menu`
VALUES ('124', '122', 'JVM信息', '/monitor/system/jvminfo', 'monitor/JvmInfo', null, null, '0', '1',
        '2019-01-18 02:33:30', '2019-01-18 02:46:51');
INSERT INTO `t_menu`
VALUES ('127', '122', '服务器信息', '/monitor/system/info', 'monitor/SystemInfo', null, null, '0', '3',
        '2019-01-21 07:53:43', '2019-01-21 07:57:00');
INSERT INTO `t_menu`
VALUES ('128', '0', '其他模块', '/others', 'PageView', null, 'coffee', '0', '5', '2019-01-22 06:49:59',
        '2019-01-22 06:50:13');
INSERT INTO `t_menu`
VALUES ('129', '128', '导入导出', '/others/excel', 'others/Excel', null, null, '0', '1', '2019-01-22 06:51:36',
        '2019-01-22 07:06:45');
INSERT INTO `t_menu`
VALUES ('130', '3', '导出Excel', null, null, 'user:export', null, '1', null, '2019-01-23 06:35:16', null);
INSERT INTO `t_menu`
VALUES ('131', '4', '导出Excel', null, null, 'role:export', null, '1', null, '2019-01-23 06:35:36', null);
INSERT INTO `t_menu`
VALUES ('132', '5', '导出Excel', null, null, 'menu:export', null, '1', null, '2019-01-23 06:36:05', null);
INSERT INTO `t_menu`
VALUES ('133', '6', '导出Excel', null, null, 'dept:export', null, '1', null, '2019-01-23 06:36:25', null);
INSERT INTO `t_menu`
VALUES ('134', '64', '导出Excel', null, null, 'dict:export', null, '1', null, '2019-01-23 06:36:43', null);
INSERT INTO `t_menu`
VALUES ('135', '3', '密码重置', null, null, 'user:reset', null, '1', null, '2019-01-23 06:37:00', null);
INSERT INTO `t_menu`
VALUES ('136', '10', '导出Excel', null, null, 'log:export', null, '1', null, '2019-01-23 06:37:27', null);
INSERT INTO `t_menu`
VALUES ('137', '102', '导出Excel', null, null, 'job:export', null, '1', null, '2019-01-23 06:37:59', null);
INSERT INTO `t_menu`
VALUES ('138', '109', '导出Excel', null, null, 'jobLog:export', null, '1', null, '2019-01-23 06:38:32', null);

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
drop table if exists `t_role`;
create table `t_role`
(
  `role_id`     bigint(20) not null auto_increment comment '角色id',
  `role_name`   varchar(10) not null comment '角色名称',
  `remark`      varchar(100) default null comment '角色描述',
  `create_time` datetime    not null comment '创建时间',
  `modify_time` datetime     default null comment '修改时间',
  primary key (`role_id`) using btree
) engine=innodb auto_increment=74 default charset=utf8 row_format=dynamic;
-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role`
VALUES ('1', '管理员', '管理员', '2017-12-27 16:23:11', '2019-01-23 06:45:29');
INSERT INTO `t_role`
VALUES ('2', '注册用户', '可查看，新增，导出', '2019-01-04 14:11:28', '2019-01-23 07:37:08');
INSERT INTO `t_role`
VALUES ('72', '普通用户', '只可查看，好可怜哦', '2019-01-23 07:33:20', null);

-- ----------------------------
-- Table structure for t_role_menu
-- ----------------------------
drop table if exists `t_role_menu`;
create table `t_role_menu`
(
  `role_id` bigint(20) not null,
  `menu_id` bigint(20) not null
) engine=innodb default charset=utf8 row_format=dynamic;
-- ----------------------------
-- Records of t_role_menu
-- ----------------------------
INSERT INTO `t_role_menu`
VALUES ('1', '1');
INSERT INTO `t_role_menu`
VALUES ('1', '3');
INSERT INTO `t_role_menu`
VALUES ('1', '11');
INSERT INTO `t_role_menu`
VALUES ('1', '12');
INSERT INTO `t_role_menu`
VALUES ('1', '13');
INSERT INTO `t_role_menu`
VALUES ('1', '4');
INSERT INTO `t_role_menu`
VALUES ('1', '14');
INSERT INTO `t_role_menu`
VALUES ('1', '15');
INSERT INTO `t_role_menu`
VALUES ('1', '16');
INSERT INTO `t_role_menu`
VALUES ('1', '5');
INSERT INTO `t_role_menu`
VALUES ('1', '17');
INSERT INTO `t_role_menu`
VALUES ('1', '18');
INSERT INTO `t_role_menu`
VALUES ('1', '19');
INSERT INTO `t_role_menu`
VALUES ('1', '6');
INSERT INTO `t_role_menu`
VALUES ('1', '20');
INSERT INTO `t_role_menu`
VALUES ('1', '21');
INSERT INTO `t_role_menu`
VALUES ('1', '22');
INSERT INTO `t_role_menu`
VALUES ('1', '64');
INSERT INTO `t_role_menu`
VALUES ('1', '65');
INSERT INTO `t_role_menu`
VALUES ('1', '66');
INSERT INTO `t_role_menu`
VALUES ('1', '67');
INSERT INTO `t_role_menu`
VALUES ('1', '2');
INSERT INTO `t_role_menu`
VALUES ('1', '8');
INSERT INTO `t_role_menu`
VALUES ('1', '23');
INSERT INTO `t_role_menu`
VALUES ('1', '10');
INSERT INTO `t_role_menu`
VALUES ('1', '24');
INSERT INTO `t_role_menu`
VALUES ('1', '113');
INSERT INTO `t_role_menu`
VALUES ('1', '121');
INSERT INTO `t_role_menu`
VALUES ('1', '122');
INSERT INTO `t_role_menu`
VALUES ('1', '124');
INSERT INTO `t_role_menu`
VALUES ('1', '123');
INSERT INTO `t_role_menu`
VALUES ('1', '125');
INSERT INTO `t_role_menu`
VALUES ('1', '101');
INSERT INTO `t_role_menu`
VALUES ('1', '102');
INSERT INTO `t_role_menu`
VALUES ('1', '103');
INSERT INTO `t_role_menu`
VALUES ('1', '104');
INSERT INTO `t_role_menu`
VALUES ('1', '105');
INSERT INTO `t_role_menu`
VALUES ('1', '106');
INSERT INTO `t_role_menu`
VALUES ('1', '107');
INSERT INTO `t_role_menu`
VALUES ('1', '108');
INSERT INTO `t_role_menu`
VALUES ('1', '109');
INSERT INTO `t_role_menu`
VALUES ('1', '110');
INSERT INTO `t_role_menu`
VALUES ('1', '58');
INSERT INTO `t_role_menu`
VALUES ('1', '59');
INSERT INTO `t_role_menu`
VALUES ('1', '61');
INSERT INTO `t_role_menu`
VALUES ('1', '81');
INSERT INTO `t_role_menu`
VALUES ('1', '82');
INSERT INTO `t_role_menu`
VALUES ('1', '83');
INSERT INTO `t_role_menu`
VALUES ('1', '127');
INSERT INTO `t_role_menu`
VALUES ('1', '128');
INSERT INTO `t_role_menu`
VALUES ('1', '129');
INSERT INTO `t_role_menu`
VALUES ('1', '130');
INSERT INTO `t_role_menu`
VALUES ('1', '135');
INSERT INTO `t_role_menu`
VALUES ('1', '131');
INSERT INTO `t_role_menu`
VALUES ('1', '132');
INSERT INTO `t_role_menu`
VALUES ('1', '133');
INSERT INTO `t_role_menu`
VALUES ('1', '134');
INSERT INTO `t_role_menu`
VALUES ('1', '136');
INSERT INTO `t_role_menu`
VALUES ('1', '137');
INSERT INTO `t_role_menu`
VALUES ('1', '138');
INSERT INTO `t_role_menu`
VALUES ('72', '1');
INSERT INTO `t_role_menu`
VALUES ('72', '3');
INSERT INTO `t_role_menu`
VALUES ('72', '4');
INSERT INTO `t_role_menu`
VALUES ('72', '5');
INSERT INTO `t_role_menu`
VALUES ('72', '6');
INSERT INTO `t_role_menu`
VALUES ('72', '64');
INSERT INTO `t_role_menu`
VALUES ('72', '2');
INSERT INTO `t_role_menu`
VALUES ('72', '8');
INSERT INTO `t_role_menu`
VALUES ('72', '10');
INSERT INTO `t_role_menu`
VALUES ('72', '113');
INSERT INTO `t_role_menu`
VALUES ('72', '121');
INSERT INTO `t_role_menu`
VALUES ('72', '122');
INSERT INTO `t_role_menu`
VALUES ('72', '124');
INSERT INTO `t_role_menu`
VALUES ('72', '123');
INSERT INTO `t_role_menu`
VALUES ('72', '127');
INSERT INTO `t_role_menu`
VALUES ('72', '101');
INSERT INTO `t_role_menu`
VALUES ('72', '102');
INSERT INTO `t_role_menu`
VALUES ('72', '109');
INSERT INTO `t_role_menu`
VALUES ('72', '58');
INSERT INTO `t_role_menu`
VALUES ('72', '59');
INSERT INTO `t_role_menu`
VALUES ('72', '61');
INSERT INTO `t_role_menu`
VALUES ('72', '81');
INSERT INTO `t_role_menu`
VALUES ('72', '82');
INSERT INTO `t_role_menu`
VALUES ('72', '83');
INSERT INTO `t_role_menu`
VALUES ('72', '128');
INSERT INTO `t_role_menu`
VALUES ('72', '129');
INSERT INTO `t_role_menu`
VALUES ('2', '3');
INSERT INTO `t_role_menu`
VALUES ('2', '1');
INSERT INTO `t_role_menu`
VALUES ('2', '4');
INSERT INTO `t_role_menu`
VALUES ('2', '5');
INSERT INTO `t_role_menu`
VALUES ('2', '6');
INSERT INTO `t_role_menu`
VALUES ('2', '64');
INSERT INTO `t_role_menu`
VALUES ('2', '2');
INSERT INTO `t_role_menu`
VALUES ('2', '8');
INSERT INTO `t_role_menu`
VALUES ('2', '10');
INSERT INTO `t_role_menu`
VALUES ('2', '113');
INSERT INTO `t_role_menu`
VALUES ('2', '121');
INSERT INTO `t_role_menu`
VALUES ('2', '122');
INSERT INTO `t_role_menu`
VALUES ('2', '124');
INSERT INTO `t_role_menu`
VALUES ('2', '123');
INSERT INTO `t_role_menu`
VALUES ('2', '125');
INSERT INTO `t_role_menu`
VALUES ('2', '101');
INSERT INTO `t_role_menu`
VALUES ('2', '102');
INSERT INTO `t_role_menu`
VALUES ('2', '109');
INSERT INTO `t_role_menu`
VALUES ('2', '58');
INSERT INTO `t_role_menu`
VALUES ('2', '59');
INSERT INTO `t_role_menu`
VALUES ('2', '61');
INSERT INTO `t_role_menu`
VALUES ('2', '81');
INSERT INTO `t_role_menu`
VALUES ('2', '82');
INSERT INTO `t_role_menu`
VALUES ('2', '83');
INSERT INTO `t_role_menu`
VALUES ('2', '127');
INSERT INTO `t_role_menu`
VALUES ('2', '128');
INSERT INTO `t_role_menu`
VALUES ('2', '129');
INSERT INTO `t_role_menu`
VALUES ('2', '130');
INSERT INTO `t_role_menu`
VALUES ('2', '14');
INSERT INTO `t_role_menu`
VALUES ('2', '17');
INSERT INTO `t_role_menu`
VALUES ('2', '132');
INSERT INTO `t_role_menu`
VALUES ('2', '20');
INSERT INTO `t_role_menu`
VALUES ('2', '133');
INSERT INTO `t_role_menu`
VALUES ('2', '65');
INSERT INTO `t_role_menu`
VALUES ('2', '134');
INSERT INTO `t_role_menu`
VALUES ('2', '136');
INSERT INTO `t_role_menu`
VALUES ('2', '103');
INSERT INTO `t_role_menu`
VALUES ('2', '137');
INSERT INTO `t_role_menu`
VALUES ('2', '138');
INSERT INTO `t_role_menu`
VALUES ('2', '131');

-- ----------------------------
-- Table structure for t_test
-- ----------------------------
drop table if exists `t_test`;
create table `t_test`
(
  `field1`      varchar(20)  not null,
  `field2`      int(11) not null,
  `field3`      varchar(100) not null,
  `create_time` datetime     not null
) engine=innodb default charset=utf8;
-- ----------------------------
-- Records of t_test
-- ----------------------------
INSERT INTO `t_test`
VALUES ('字段1', '1', 'mrbird@gmail.com', '2019-01-22 16:26:51');
INSERT INTO `t_test`
VALUES ('字段1', '1', 'mrbird0@gmail.com', '2019-01-23 03:01:03');
INSERT INTO `t_test`
VALUES ('字段1', '2', 'mrbird1@gmail.com', '2019-01-23 03:01:03');
INSERT INTO `t_test`
VALUES ('字段1', '3', 'mrbird2@gmail.com', '2019-01-23 03:01:03');
INSERT INTO `t_test`
VALUES ('字段1', '4', 'mrbird3@gmail.com', '2019-01-23 03:01:03');
INSERT INTO `t_test`
VALUES ('字段1', '5', 'mrbird4@gmail.com', '2019-01-23 03:01:03');
INSERT INTO `t_test`
VALUES ('字段1', '10', 'mrbird9@gmail.com', '2019-01-23 03:01:03');
INSERT INTO `t_test`
VALUES ('字段1', '1', 'mrbird0@gmail.com', '2019-01-23 03:03:49');
INSERT INTO `t_test`
VALUES ('字段1', '2', 'mrbird1@gmail.com', '2019-01-23 03:03:49');
INSERT INTO `t_test`
VALUES ('字段1', '3', 'mrbird2@gmail.com', '2019-01-23 03:03:49');
INSERT INTO `t_test`
VALUES ('字段1', '4', 'mrbird3@gmail.com', '2019-01-23 03:03:49');
INSERT INTO `t_test`
VALUES ('字段1', '5', 'mrbird4@gmail.com', '2019-01-23 03:03:49');
INSERT INTO `t_test`
VALUES ('字段1', '10', 'mrbird9@gmail.com', '2019-01-23 03:03:49');
INSERT INTO `t_test`
VALUES ('字段1', '1', 'mrbird0@gmail.com', '2019-01-23 03:07:47');
INSERT INTO `t_test`
VALUES ('字段1', '2', 'mrbird1@gmail.com', '2019-01-23 03:07:47');
INSERT INTO `t_test`
VALUES ('字段1', '3', 'mrbird2@gmail.com', '2019-01-23 03:07:47');
INSERT INTO `t_test`
VALUES ('字段1', '4', 'mrbird3@gmail.com', '2019-01-23 03:07:47');
INSERT INTO `t_test`
VALUES ('字段1', '5', 'mrbird4@gmail.com', '2019-01-23 03:07:47');
INSERT INTO `t_test`
VALUES ('字段1', '6', 'mrbird5@gmail.com', '2019-01-23 03:07:47');
INSERT INTO `t_test`
VALUES ('字段1', '1', 'mrbird0@gmail.com', '2019-01-23 03:08:09');
INSERT INTO `t_test`
VALUES ('字段1', '2', 'mrbird1@gmail.com', '2019-01-23 03:08:09');
INSERT INTO `t_test`
VALUES ('字段1', '3', 'mrbird2@gmail.com', '2019-01-23 03:08:09');
INSERT INTO `t_test`
VALUES ('字段1', '4', 'mrbird3@gmail.com', '2019-01-23 03:08:09');
INSERT INTO `t_test`
VALUES ('字段1', '5', 'mrbird4@gmail.com', '2019-01-23 03:08:09');
INSERT INTO `t_test`
VALUES ('字段1', '10', 'mrbird9@gmail.com', '2019-01-23 03:08:09');
INSERT INTO `t_test`
VALUES ('字段1', '1', 'mrbird0@gmail.com', '2019-01-23 03:19:52');
INSERT INTO `t_test`
VALUES ('字段1', '2', 'mrbird1@gmail.com', '2019-01-23 03:19:52');
INSERT INTO `t_test`
VALUES ('字段1', '3', 'mrbird2@gmail.com', '2019-01-23 03:19:52');
INSERT INTO `t_test`
VALUES ('字段1', '4', 'mrbird3@gmail.com', '2019-01-23 03:19:52');
INSERT INTO `t_test`
VALUES ('字段1', '5', 'mrbird4@gmail.com', '2019-01-23 03:19:52');
INSERT INTO `t_test`
VALUES ('字段1', '6', 'mrbird5@gmail.com', '2019-01-23 03:19:52');
INSERT INTO `t_test`
VALUES ('字段1', '1', 'mrbird0@gmail.com', '2019-01-23 03:20:56');
INSERT INTO `t_test`
VALUES ('字段1', '2', 'mrbird1@gmail.com', '2019-01-23 03:20:56');
INSERT INTO `t_test`
VALUES ('字段1', '3', 'mrbird2@gmail.com', '2019-01-23 03:20:56');
INSERT INTO `t_test`
VALUES ('字段1', '4', 'mrbird3@gmail.com', '2019-01-23 03:20:56');
INSERT INTO `t_test`
VALUES ('字段1', '5', 'mrbird4@gmail.com', '2019-01-23 03:20:56');
INSERT INTO `t_test`
VALUES ('字段1', '6', 'mrbird5@gmail.com', '2019-01-23 03:20:56');
INSERT INTO `t_test`
VALUES ('字段1', '1', 'mrbird0@gmail.com', '2019-01-23 06:12:38');
INSERT INTO `t_test`
VALUES ('字段1', '2', 'mrbird1@gmail.com', '2019-01-23 06:12:38');
INSERT INTO `t_test`
VALUES ('字段1', '3', 'mrbird2@gmail.com', '2019-01-23 06:12:38');
INSERT INTO `t_test`
VALUES ('字段1', '4', 'mrbird3@gmail.com', '2019-01-23 06:12:38');
INSERT INTO `t_test`
VALUES ('字段1', '5', 'mrbird4@gmail.com', '2019-01-23 06:12:38');
INSERT INTO `t_test`
VALUES ('字段1', '6', 'mrbird5@gmail.com', '2019-01-23 06:12:38');
INSERT INTO `t_test`
VALUES ('字段1', '7', 'mrbird6@gmail.com', '2019-01-23 06:12:38');
INSERT INTO `t_test`
VALUES ('字段1', '8', 'mrbird7@gmail.com', '2019-01-23 06:12:38');
INSERT INTO `t_test`
VALUES ('字段1', '9', 'mrbird8@gmail.com', '2019-01-23 06:12:38');
INSERT INTO `t_test`
VALUES ('字段1', '11', 'mrbird10@gmail.com', '2019-01-23 06:12:38');
INSERT INTO `t_test`
VALUES ('字段1', '12', 'mrbird11@gmail.com', '2019-01-23 06:12:38');
INSERT INTO `t_test`
VALUES ('字段1', '14', 'mrbird13@gmail.com', '2019-01-23 06:12:38');
INSERT INTO `t_test`
VALUES ('字段1', '15', 'mrbird14@gmail.com', '2019-01-23 06:12:38');
INSERT INTO `t_test`
VALUES ('字段1', '16', 'mrbird15@gmail.com', '2019-01-23 06:12:38');
INSERT INTO `t_test`
VALUES ('字段1', '18', 'mrbird17@gmail.com', '2019-01-23 06:12:38');
INSERT INTO `t_test`
VALUES ('字段1', '19', 'mrbird18@gmail.com', '2019-01-23 06:12:38');
INSERT INTO `t_test`
VALUES ('字段1', '20', 'mrbird19@gmail.com', '2019-01-23 06:12:38');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
drop table if exists `t_user`;
create table `t_user`
(
  `user_id`         bigint(10) not null auto_increment comment '用户id',
  `username`        varchar(50)  not null comment '用户名',
  `password`        varchar(128) not null comment '密码',
  `dept_id`         bigint(20) default null comment '部门id',
  `email`           varchar(128) default null comment '邮箱',
  `mobile`          varchar(20)  default null comment '联系电话',
  `status`          char(1)      not null comment '状态 0锁定 1有效',
  `create_time`     datetime     not null comment '创建时间',
  `modify_time`     datetime     default null comment '修改时间',
  `last_login_time` datetime     default null comment '最近访问时间',
  `ssex`            char(1)      default null comment '性别 0男 1女 2保密',
  `description`     varchar(100) default null comment '描述',
  `avatar`          varchar(100) default null comment '用户头像',
  primary key (`user_id`) using btree
) engine=innodb auto_increment=13 default charset=utf8 row_format=dynamic;
-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user`
VALUES ('1', 'mrbird', '94f860c4bbfeb2f49c84e321fdda4b07', '1', 'mrbird123@hotmail.com', '13455533233', '1',
        '2017-12-27 15:47:19', '2019-01-17 02:34:19', '2019-01-28 01:53:58', '2', '我是帅比作者。',
        'ubnKSIfAJTxIgXOKlciN.png');
INSERT INTO `t_user`
VALUES ('2', 'scott', '7b44a5363e3fd52435beb472e1d2b91f', '6', 'scott@qq.com', '15134627380', '1',
        '2017-12-29 16:16:39', '2019-01-18 00:59:09', '2019-01-28 01:54:09', '0', '我是scott，嗯嗯',
        'jZUIxmJycoymBprLOUbT.png');
INSERT INTO `t_user`
VALUES ('12', 'jack', '552649f10640385d0728a80a4242893e', '6', 'jack@hotmail.com', null, '1', '2019-01-23 07:34:05',
        '2019-01-24 03:08:01', '2019-01-24 08:52:03', '0', null, 'default.jpg');

-- ----------------------------
-- Table structure for t_user_config
-- ----------------------------
drop table if exists `t_user_config`;
create table `t_user_config`
(
  `user_id`      bigint(20) not null comment '用户id',
  `theme`        varchar(10) default null comment '系统主题 dark暗色风格，light明亮风格',
  `layout`       varchar(10) default null comment '系统布局 side侧边栏，head顶部栏',
  `multi_page`   char(1)     default null comment '页面风格 1多标签页 0单页',
  `fix_siderbar` char(1)     default null comment '页面滚动是否固定侧边栏 1固定 0不固定',
  `fix_header`   char(1)     default null comment '页面滚动是否固定顶栏 1固定 0不固定',
  `color`        varchar(20) default null comment '主题颜色 rgb值',
  primary key (`user_id`) using btree
) engine=innodb default charset=utf8 row_format=dynamic;

-- ----------------------------
-- Records of t_user_config
-- ----------------------------
INSERT INTO `t_user_config`
VALUES ('1', 'light', 'side', '1', '1', '1', 'rgb(24, 144, 255)');
INSERT INTO `t_user_config`
VALUES ('2', 'light', 'head', '0', '1', '1', 'rgb(24, 144, 255)');
INSERT INTO `t_user_config`
VALUES ('12', 'dark', 'side', '1', '1', '1', 'rgb(66, 185, 131)');

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
drop table if exists `t_user_role`;
create table `t_user_role`
(
  `user_id` bigint(20) not null comment '用户id',
  `role_id` bigint(20) not null comment '角色id'
) engine=innodb default charset=utf8 row_format=dynamic;
-- ----------------------------
-- Records of t_user_role
-- ----------------------------
INSERT INTO `t_user_role`
VALUES ('1', '1');
INSERT INTO `t_user_role`
VALUES ('2', '2');
INSERT INTO `t_user_role`
VALUES ('12', '72');


