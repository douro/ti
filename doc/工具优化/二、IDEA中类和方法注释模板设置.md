## 二、IDEA中类和方法注释模板设置

一、首先我们来设置IDEA中类的模板：（IDEA中在创建类时会自动给添加注释）

1、File-->settings-->Editor-->File and Code Templates-->Includes

~~~java
/**
 * TODO
 * @author ${USER}
 * @date ${DATE} ${TIME}
 * @versiong 1.0
 */
~~~

![设置类模版.png](https://upload.cc/i1/2019/08/22/wni5cQ.png)

二、设置方法注释模板

IDEA还没有智能到自动为我们创建方法注释，这就是要我们手动为方法添加注释，使用Eclipse时我们生成注释的习惯是/**+Enter，这里我们也按照这种习惯来设置IDEA的方法注释。

1、File-->Settings-->Editor-->Live Templates

![新建组或者模版.png](https://upload.cc/i1/2019/08/22/Lf7ckn.png)

（1）新建组：命名为：userDefine

（2）新建模板：命名为*

因为IDEA生成注释的默认方式是：/*+模板名+快捷键（比如若设置模板名为add快捷键用Tab，则生成方式为

/*add+Tab），如果不采用这样的生成方式IDEA中没有内容的方法将不可用，例如获取方法参数的methodParameters(）、获取方法返回值的methodReturnType(）

![新建模版.png](https://upload.cc/i1/2019/08/22/GT8SkD.png)

（3）设置生成注释的快捷键

![设置生成注释的快捷键.png](https://upload.cc/i1/2019/08/22/dhv86P.png)

（4）设置模板：模板内容如下：

注意第一行，只有一个*而不是/*

在设置参数名时必须用${参数名}$的方式，否则第五步中读取不到你设置的参数名

~~~java
*
 * TODO(这里用一句话描述这个方法的作用)
 * @Date $date$ $time$
 * @Param $param$
 * @return $return$
 **/
~~~

![设置模板.png](https://upload.cc/i1/2019/08/22/zTOmBH.png)

（5）设置模板的应用场景

点击模板页面最下方的警告，来设置将模板应用于那些场景，一般选择EveryWhere-->Java即可

（如果曾经修改过，则显示为change而不是define）

![设置模板的应用场景.png](https://upload.cc/i1/2019/08/22/T0wEJA.png)

（6）设置参数的获取方式

选择右侧的Edit variables按钮

PS:第五步和第六步顺序不可颠倒，否则第六步将获取不到方法

![设置参数的获取方式.png](https://upload.cc/i1/2019/08/22/Qla8jD.png)

（7）效果图

创建方法，在方法上面写：/*+模板名+Enter-->/**+Enter

![效果图.png](https://upload.cc/i1/2019/08/22/7hC58O.png)