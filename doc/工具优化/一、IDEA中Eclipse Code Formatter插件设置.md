## 一、IDEA中Eclipse Code Formatter插件设置

在settings里面找到plugins这个选项，搜索Eclipse Code Formatter，点击安装，重启idea即可进行配置；

首先，先安装Eclipse Code Formatter插件：

![插件安装.png](https://upload.cc/i1/2019/08/14/7Igy4L.png)

重启idea后，进行设置：

![插件设置.png](https://upload.cc/i1/2019/08/14/uoJg7T.png)

选择我们这导入的格式化模板：

![导入模版.png](https://upload.cc/i1/2019/08/14/dwvMPq.png)

格式化快捷键：Ctrl+Shift+L