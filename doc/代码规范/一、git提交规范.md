## 一、git提交规范

**1、 前言**

一个好的项目通常都是多人合作的结果，然而每个人有每个人的开发习惯，并不统一。

所以 commit message 就显得格外的重要。有些不规范的 commit 可能过个一个月之后你自己都不知道当时的提交目的了ﾍ(;´Д｀ﾍ)，

所以，为了能使得日后复（zhao）盘（guo）的时候更加的方便，团队之间遵守同一套 commit message 规范还是很有必要的。

**2、 目的**

- **统一团队 Git commit 日志标准，便于后续代码 review，版本发布以及日志自动化生成等等**。
- **统一团队的 Git 工作流，包括分支使用、tag 规范、issue 等**

**3、 规范介绍**

这次主要介绍 AngularJS 的规范，它是由 Google 推出的一套提交消息规范标准，也是目前使
用范围最广的规范。有一套合理的[手册](https://docs.google.com/document/d/1QrDFcIiPjSLDn3EL15IJygNPiHORgU1_OOAqWjiDU5Y/edit)也较为[系统化](https://github.com/angular/angular.js/blob/master/CONTRIBUTING.md#toc10)；并且还有配套的工具可以供我们使用。

说白了，规范就是用工具进行强约束。单看规范比较简单，所以先让大家先看看面，知道他的
大体规则后，在来讲细节。

规范执行方案如下：　　

![git-commit-message-mindmap.png](https://upload.cc/i1/2019/08/26/kpNlMs.png)

### 4、Git commit日志基本规范

~~~xml
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
~~~

**所有的 type 类型如下：**

> type代表某次提交的类型，比如是修复一个bug还是增加一个新的feature。

* feat： 新增 feature
* fix: 修复 bug
* docs: 仅仅修改了文档，比如 README, CHANGELOG, CONTRIBUTE等等
* style: 仅仅修改了空格、格式缩进、逗号等等，不改变代码逻辑
* refactor: 代码重构，没有加新功能或者修复 bug
* perf: 优化相关，比如提升性能、体验
* test: 测试用例，包括单元测试、集成测试等
* chore: 改变构建流程、或者增加依赖库、工具等
* revert: 回滚到上一个版本

**5、格式要求：**

```
# 标题行：50个字符以内，描述主要变更内容
#
# 主体内容：更详细的说明文本，建议72个字符以内。 需要描述的信息包括:
#
# * 为什么这个变更是必须的? 它可能是用来修复一个bug，增加一个feature，提升性能、可靠性、稳定性等等
# * 他如何解决这个问题? 具体描述解决问题的步骤
# * 是否存在副作用、风险? 
#
# 尾部：如果需要的化可以添加一个链接到issue地址或者其它文档，或者关闭某个issue。
```

### 6、Git分支与版本发布规范

* 基本原则：master为保护分支，不直接在master上进行代码修改和提交。
* 开发日常需求或者项目时，从master分支上checkout一个feature分支进行开发或者bugfix分支进行bug修复，功能测试完毕并且项目发布上线后，`将feature分支合并到主干master，并且打Tag发布，最后删除开发分支`。分支命名规范：
    * 分支版本命名规则：分支类型 _ 分支发布时间 _ 分支功能。比如：feat_20170401_fairy_flower
    * 分支类型包括：feat、 fix、refactor三种类型，即新功能开发、bug修复和代码重构
    * 时间使用年月日进行命名，不足2位补0
    * 分支功能命名使用snake case命名法，即下划线命名。
* Tag包括3位版本，前缀使用v。比如v1.2.31。Tag命名规范：
    * 新功能开发使用第2位版本号，bug修复使用第3位版本号
    * 核心基础库或者Node中间价可以在大版本发布请使用灰度版本号，在版本后面加上后缀，用中划线分隔。alpha或者belta后面加上次数，即第几次alpha：
        * v2.0.0-alpha.1 
        * v2.0.0-belta.1
* 版本正式发布前需要生成changelog文档，然后再发布上线。

### 7、开发工具设置

Install directly from the IDE plugin manager (File > Settings > Plugins > Browser repositories > Git Commit Template)

### 8、使用（对应提交模版位于项目目录/template_git.txt）

![commit-template-1.png](https://upload.cc/i1/2019/08/26/nzLe0y.png)

![commit-template-2.png](https://upload.cc/i1/2019/08/26/jPoKcr.png)

![commit-template-3.png](https://upload.cc/i1/2019/08/26/xGn9Bg.png)

### 9、参考链接

https://github.com/MobileTribe/commit-template-idea-plugin

https://github.com/feflow/git-commit-style-guide

https://github.com/ctaodream/template_git/blob/master/.template_git

https://www.cnblogs.com/wubaiqing/p/10307605.html

https://juejin.im/post/5bd2debfe51d457abc710b57

https://segmentfault.com/a/1190000009048911

https://blog.csdn.net/u011870547/article/details/81203419

http://blog.didispace.com/git-commit-specification/