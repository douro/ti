package com.ti.core.converter;

import com.ti.core.utils.DateUtils;
import com.wuwenze.poi.convert.WriteConverter;
import com.wuwenze.poi.exception.ExcelKitWriteConverterException;

import lombok.extern.slf4j.Slf4j;

/**
 * Excel导出时间类型字段格式化
 *
 * @author Lord
 * @date 2019/8/22 13:56
 */
@Slf4j
public class TimeConverter implements WriteConverter {

    @Override
    public String convert(Object value) throws ExcelKitWriteConverterException {

        try {
            if (value == null) {
                return "";
            } else {
                return DateUtils.formatCSTTime(value.toString(), DateUtils.FULL_TIME_SPLIT_PATTERN);
            }
        } catch (Exception e) {
            log.error("时间转换异常", e);
            return "";
        }
    }

}
