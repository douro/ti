package com.ti.core.exception;

import org.apache.shiro.authc.AuthenticationException;

/**
 * Token 过期异常
 *
 * @author Lord
 * @date 2019/8/22 13:56
 */
public class TokenTimeoutException extends AuthenticationException {

    private static final long serialVersionUID = -8313101744886192005L;

    public TokenTimeoutException(String message) {
        super(message);
    }
}
