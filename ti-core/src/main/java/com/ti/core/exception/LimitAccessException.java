package com.ti.core.exception;

/**
 * 限流异常
 *
 * @author Lord
 * @date 2019/8/22 13:56
 */
public class LimitAccessException extends Exception {

    private static final long serialVersionUID = -3608667856397125671L;

    public LimitAccessException(String message) {
        super(message);
    }
}
