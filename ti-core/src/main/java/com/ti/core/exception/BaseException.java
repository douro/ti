package com.ti.core.exception;

/**
 * 系统内部异常
 *
 * @author Lord
 * @date 2019/8/22 13:56
 */
public class BaseException extends Exception {

    private static final long serialVersionUID = -994962710559017255L;

    public BaseException(String message) {
        super(message);
    }
}
