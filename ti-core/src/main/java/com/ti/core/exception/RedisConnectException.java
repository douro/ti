package com.ti.core.exception;

/**
 * Redis 连接异常
 *
 * @author Lord
 * @date 2019/8/22 13:56
 */
public class RedisConnectException extends Exception {

    private static final long serialVersionUID = 1639374111871115063L;

    public RedisConnectException(String message) {
        super(message);
    }
}
