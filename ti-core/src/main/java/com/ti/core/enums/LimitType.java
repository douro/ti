package com.ti.core.enums;

/**
 * 接口限流枚举
 *
 * @author Lord
 * @date 2019/8/22 13:56
 */
public enum LimitType {

    // 传统类型
    CUSTOMER,

    // 根据 IP 限制
    IP
}
