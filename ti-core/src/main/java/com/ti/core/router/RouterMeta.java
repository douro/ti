package com.ti.core.router;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * Vue路由 Meta
 *
 * @author Lord
 * @date 2019/8/22 13:56
 */
@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RouterMeta implements Serializable {

    private static final long serialVersionUID = 5499925008927195914L;

    /**
     * 开关
     */
    private Boolean closeable;

    /**
     * 是否显示
     */
    private Boolean isShow;
}
