package com.ti.core.base;

import com.ti.core.utils.JsonUtils;

import java.io.Serializable;

/**
 * Model层基类
 *
 * @author Lord
 * @date 2019/8/22 13:56
 */
public abstract class BaseModel<E extends BaseModel<E>> implements Serializable {

    private static final long serialVersionUID = 7279175797938975527L;

    @Override
    public String toString() {
        return toJson();
    }

    public String toJson() {
        return JsonUtils.toJson(this);
    }

    protected String trim(String str) {
        return str != null ? str.trim() : null;
    }

    protected String clip(String str) {
        return str != null ? str.trim() : "";
    }
}
