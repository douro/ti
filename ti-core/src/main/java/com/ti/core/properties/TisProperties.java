package com.ti.core.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 系统配置
 *
 * @author Lord
 * @date 2019/8/22 13:56
 */
@Data
@ConfigurationProperties(prefix = "tis")
public class TisProperties {

    private ShiroProperties shiro = new ShiroProperties();
    private boolean openAopLog = true;
}
