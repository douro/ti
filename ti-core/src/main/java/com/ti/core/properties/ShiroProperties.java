package com.ti.core.properties;

import lombok.Data;

/**
 * 安全框架配置
 *
 * @author Lord
 * @date 2019/8/22 13:56
 */
@Data
public class ShiroProperties {

    private String anonUrl;

    /**
     * token默认有效时间 1天
     */
    private Long jwtTimeOut = 86400L;
}
