package com.ti.core.lang;

/**
 * 正则常量
 *
 * @author Lord
 * @date 2019/8/22 13:56
 */
public class RegexpConstant {

    /**
     * 简单手机号正则（这里只是简单校验是否为11位，实际规则很复杂）
     */
    public static final String MOBILE_REG = "[1]\\d{10}";
}
