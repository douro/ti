package com.ti.core.domain;

import java.util.HashMap;

/**
 * 响应提示对象
 *
 * @author Lord
 * @date 2019/8/22 13:56
 */
public class TisResponse extends HashMap<String, Object> {

    private static final long serialVersionUID = -8713837118340960775L;

    public TisResponse message(String message) {
        this.put("message", message);
        return this;
    }

    public TisResponse data(Object data) {
        this.put("data", data);
        return this;
    }

    @Override
    public TisResponse put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}
