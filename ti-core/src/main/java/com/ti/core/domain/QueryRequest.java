package com.ti.core.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * 检索对象
 *
 * @author Lord
 * @date 2019/8/22 13:56
 */
@Data
public class QueryRequest implements Serializable {

    private static final long serialVersionUID = -4869594085374385813L;

    /**
     * 当前页面数据量
     */
    private int pageSize = 10;

    /**
     * 当前页码
     */
    private int pageNum = 1;

    /**
     * 排序字段
     */
    private String sortField;

    /**
     * 排序规则，asc升序，desc降序
     */
    private String sortOrder;
}
