package com.ti.core.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ti.core.support.jackson.JacksonObjectMapper;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Json工具类
 *
 * @author Lord
 * @date 2019/8/22 13:56
 */
public abstract class JsonUtils {

    private static final ObjectMapper objectMapper = new JacksonObjectMapper();

    public static String toJson(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (Exception e) {
            return "{}";
        }
    }

    public static <T> T toObject(String modelJson, Class<T> clazz) {
        try {
            return objectMapper.readValue(modelJson, clazz);
        } catch (Exception e) {
            return null;
        }
    }

    public static <T> T[] toArray(String modelJson, Class<T[]> clazz) {
        try {
            return objectMapper.readValue(modelJson, clazz);
        } catch (Exception e) {
            return null;
        }
    }

    public static <T> List<T> toList(String modelJson, Class<T[]> clazz) {
        try {
            return Arrays.asList(toArray(modelJson, clazz));
        } catch (Exception e) {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public static List<Map<String, Object>> toList(String modelJson) {
        try {
            return objectMapper.readValue(modelJson, List.class);
        } catch (Exception e) {
            return null;
        }
    }

    public static Map<String, Object> toMap(String modelJson) {
        try {
            return objectMapper.readValue(modelJson, new TypeReference<Map<String, Object>>() {
            });
        } catch (Exception e) {
            return null;
        }
    }

    public static ObjectMapper getObjectMapper() {
        return objectMapper;
    }
}
