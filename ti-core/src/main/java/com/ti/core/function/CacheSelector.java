package com.ti.core.function;

/**
 * Cache选择器
 *
 * @author Lord
 * @date 2019/8/22 13:56
 */
@FunctionalInterface
public interface CacheSelector<T> {
    T select() throws Exception;
}
