package com.ti.core.function;

import com.ti.core.exception.RedisConnectException;

/**
 * Jedis执行器
 *
 * @author Lord
 * @date 2019/8/22 13:56
 */
@FunctionalInterface
public interface JedisExecutor<T, R> {
    R excute(T t) throws RedisConnectException;
}
