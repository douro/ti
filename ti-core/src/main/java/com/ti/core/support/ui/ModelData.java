package com.ti.core.support.ui;

import org.springframework.ui.ModelMap;

public class ModelData extends ModelMap {

    private static final long serialVersionUID = 4371406607685865487L;

    public String getStr(String key) {
        return containsKey(key) ? get(key).toString() : null;
    }

    public Integer getInt(String key) {
        return containsKey(key) && get(key) != null ? Integer.parseInt(getStr(key)) : null;
    }

    public Long getLong(String key) {
        return containsKey(key) && get(key) != null ? Long.parseLong(getStr(key)) : null;
    }

    public Double getDouble(String key) {
        return containsKey(key) && get(key) != null ? Double.parseDouble(getStr(key)) : null;
    }

    public Boolean getBool(String key) {
        return containsKey(key) && get(key) != null ? Boolean.parseBoolean(getStr(key)) : null;
    }
}
