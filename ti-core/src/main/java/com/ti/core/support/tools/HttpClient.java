package com.ti.core.support.tools;

import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

public class HttpClient {

    private String url;

    private String params;

    private HttpMethod method = HttpMethod.POST;

    private MediaType mediaType = MediaType.APPLICATION_JSON_UTF8;

    public HttpClient(String url) {
        this.url = url;
    }

    public HttpClient(String url, String params) {
        this(url);
        this.params = params;
    }

    public String requestBody() {
        ResponseEntity<String> responseEntity = request(String.class);

        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            return responseEntity.getBody();
        }

        return null;
    }

    public <T> ResponseEntity<T> request(Class<T> clazz) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(this.mediaType);

        HttpEntity<String> httpEntity = new HttpEntity<String>(this.params, httpHeaders);
        ResponseEntity<T> responseEntity = restTemplate.exchange(this.url, this.method, httpEntity, clazz);

        return responseEntity;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public HttpMethod getMethod() {
        return method;
    }

    public void setMethod(HttpMethod method) {
        this.method = method;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }
}
