package com.ti.cms.task;

import com.ti.cms.web.system.service.RedisService;
import com.ti.core.lang.TisConstant;
import com.ti.core.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * 主要用于定时删除 Redis中 key为 tis.user.active 中 已经过期的 score
 *
 * @author Lord
 * @date 2019/8/23 15:16
 */
@Slf4j
@Component
public class CacheTask {

    @Autowired
    private RedisService redisService;

    @Scheduled(fixedRate = 3600000)
    public void run() {
        try {
            String now = DateUtils.formatFullTime(LocalDateTime.now());
            redisService.zremrangeByScore(TisConstant.ACTIVE_USERS_ZSET_PREFIX, "-inf", now);
            log.info("delete expired user");
        } catch (Exception ignore) {
        }
    }
}
