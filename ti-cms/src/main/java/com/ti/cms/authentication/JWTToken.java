package com.ti.cms.authentication;

import org.apache.shiro.authc.AuthenticationToken;

import lombok.Data;

/**
 * JSON Web Token
 *
 * @author Lord
 * @date 2019/8/22 12:46
 */
@Data
public class JWTToken implements AuthenticationToken {

    private static final long serialVersionUID = 1282057025599826155L;

    /**
     * 秘钥
     */
    private String token;

    /**
     * 过期时间
     */
    private String expireAt;

    public JWTToken(String token) {
        this.token = token;
    }

    public JWTToken(String token, String expireAt) {
        this.token = token;
        this.expireAt = expireAt;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }

}
