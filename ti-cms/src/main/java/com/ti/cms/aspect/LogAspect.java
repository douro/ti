package com.ti.cms.aspect;

import com.ti.cms.web.system.domain.SysLog;
import com.ti.cms.web.system.service.LogService;
import com.ti.cms.utils.JWTUtils;
import com.ti.core.properties.TisProperties;
import com.ti.core.utils.HttpContextUtils;
import com.ti.core.utils.IPUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * AOP 记录用户操作日志
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
@Slf4j
@Aspect
@Component
public class LogAspect {

    @Autowired
    private TisProperties properties;

    @Autowired
    private LogService logService;

    @Pointcut("@annotation(com.ti.cms.annotation.Log)")
    public void pointcut() {
        // do nothing
    }

    @Around("pointcut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {

        Object result;
        long beginTime = System.currentTimeMillis();

        // 执行方法
        result = point.proceed();

        // 获取 request
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();

        // 设置 IP 地址
        String ip = IPUtils.getIpAddr(request);

        // 执行时长(毫秒)
        long time = System.currentTimeMillis() - beginTime;

        // 保存日志
        if (properties.isOpenAopLog()) {
            String token = (String) SecurityUtils.getSubject().getPrincipal();
            String username = "";
            if (StringUtils.isNotBlank(token)) {
                username = JWTUtils.getUsername(token);
            }

            SysLog log = new SysLog();
            log.setUsername(username);
            log.setIp(ip);
            log.setTime(time);
            logService.saveLog(point, log);
        }
        return result;
    }
}
