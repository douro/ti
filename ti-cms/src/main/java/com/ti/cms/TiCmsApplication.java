package com.ti.cms;

import com.ti.core.properties.TisProperties;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@EnableScheduling
@EnableAsync
@ComponentScan(basePackages = {"com.ti.cms", "com.ti.core"})
@MapperScan(value = {"com.ti.cms.*.*.dao"})
@EnableConfigurationProperties({TisProperties.class})
public class TiCmsApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(TiCmsApplication.class)
                .run(args);
    }

}
