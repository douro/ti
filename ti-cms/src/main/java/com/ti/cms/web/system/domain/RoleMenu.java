package com.ti.cms.web.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ti.core.base.BaseModel;

import lombok.Data;

/**
 * 角色菜单信息模型
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
@Data
@TableName("t_role_menu")
public class RoleMenu extends BaseModel<RoleMenu> {
	
	private static final long serialVersionUID = -7573904024872252113L;

    private Long roleId;

    private Long menuId;
}
