package com.ti.cms.web.job.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ti.cms.web.job.domain.Job;
import com.ti.core.domain.QueryRequest;

/**
 * 调度任务逻辑服务层
 *
 * @author Lord
 * @date 2019/8/23 15:16
 */
public interface JobService extends IService<Job> {

    /**
     * 获取调度任务详情
     *
     * @param jobId 调度任务主键
     * @return
     * @see Job
     */
    Job findJob(Long jobId);

    /**
     * 获取调度任务列表
     *
     * @param request 检索条件
     * @param job 调度任务信息
     * @return
     * @see com.baomidou.mybatisplus.core.metadata.IPage< Job >
     */
    IPage<Job> findJobs(QueryRequest request, Job job);

    /**
     * 新增调度任务
     *
     * @param job 调度任务信息
     * @return
     */
    void createJob(Job job);

    /**
     * 编辑调度任务
     *
     * @param job 调度任务信息
     * @return
     */
    void updateJob(Job job);

    /**
     * 删除调度任务
     *
     * @param jobIds 调度任务主键集合
     * @return
     */
    void deleteJobs(String[] jobIds);

    /**
     * 批量编辑调度任务
     *
     * @param jobIds 调度任务主键集合
     * @param status 状态
     * @return 操作影响行数
     */
    int updateBatch(String jobIds, String status);

    /**
     * 执行调度任务
     *
     * @param jobIds 调度任务主键集合
     * @return
     */
    void run(String jobIds);

    /**
     * 暂停调度任务
     *
     * @param jobIds 调度任务主键集合
     * @return
     */
    void pause(String jobIds);

    /**
     * 恢复调度任务
     *
     * @param jobIds 调度任务主键集合
     * @return
     */
    void resume(String jobIds);

}
