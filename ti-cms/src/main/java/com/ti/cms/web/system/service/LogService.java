package com.ti.cms.web.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.ti.cms.web.system.domain.SysLog;
import com.ti.core.domain.QueryRequest;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.scheduling.annotation.Async;

/**
 * 操作日志服务层
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
public interface LogService extends IService<SysLog> {

    /**
     * 获取日志列表
     *
     * @param request 分页对象
     * @param sysLog 系统日志信息
     * @return IPage<SysLog>
     */
    IPage<SysLog> findLogs(QueryRequest request, SysLog sysLog);

    /**
     * 删除日志
     *
     * @param logIds 日志id集合
     */
    void deleteLogs(String[] logIds);

    /**
     * 保存日志
     *
     * @param point
     * @param log 系统日志信息
     * @throws JsonProcessingException
     */
    @Async
    void saveLog(ProceedingJoinPoint point, SysLog log) throws JsonProcessingException;
}
