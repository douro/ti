package com.ti.cms.web.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ti.cms.web.system.domain.Test;

/**
 * 导入导出测试表Mapper
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
public interface TestMapper extends BaseMapper<Test> {

}
