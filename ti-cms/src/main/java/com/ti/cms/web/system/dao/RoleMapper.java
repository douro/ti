package com.ti.cms.web.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ti.cms.web.system.domain.Role;

import java.util.List;

/**
 * 角色表Mapper
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
public interface RoleMapper extends BaseMapper<Role> {

	/**
	 * 查找用户角色
	 *
	 * @param userName 用户账号
	 * @return 角色集合
	 */
	List<Role> findUserRole(String userName);
	
}
