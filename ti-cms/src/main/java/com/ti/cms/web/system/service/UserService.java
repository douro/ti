package com.ti.cms.web.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ti.cms.web.system.domain.User;
import com.ti.core.domain.QueryRequest;

/**
 * 用户管理服务层
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
public interface UserService extends IService<User> {

    /**
     * 通过用户名查找用户
     *
     * @param username 用户名
     * @return user
     */
    User findByName(String username);

    /**
     * 查询用户详情，包括基本信息，用户角色，用户部门
     *
     * @param user 个人信息
     * @param queryRequest queryRequest
     * @return IPage
     */
    IPage<User> findUserDetail(User user, QueryRequest queryRequest);

    /**
     * 更新用户登录时间
     *
     * @param username 用户名
     * @throws Exception
     */
    void updateLoginTime(String username) throws Exception;

    /**
     * 新增用户
     *
     * @param user 个人信息
     * @throws Exception
     */
    void createUser(User user) throws Exception;

    /**
     * 修改用户
     *
     * @param user 个人信息
     * @throws Exception
     */
    void updateUser(User user) throws Exception;

    /**
     * 删除用户
     *
     * @param userIds 用户 id数组
     * @throws Exception
     */
    void deleteUsers(String[] userIds) throws Exception;

    /**
     * 更新个人信息
     *
     * @param user 个人信息
     * @throws Exception
     */
    void updateProfile(User user) throws Exception;

    /**
     * 更新用户头像
     *
     * @param username 用户名
     * @param avatar   用户头像
     * @throws Exception
     */
    void updateAvatar(String username, String avatar) throws Exception;

    /**
     * 更新用户密码
     *
     * @param username 用户名
     * @param password 新密码
     * @throws Exception
     */
    void updatePassword(String username, String password) throws Exception;

    /**
     * 注册用户
     *
     * @param username 用户名
     * @param password 密码
     * @throws Exception
     */
    void regist(String username, String password) throws Exception;

    /**
     * 重置密码
     *
     * @param usernames 用户集合
     * @throws Exception
     */
    void resetPassword(String[] usernames) throws Exception;

}
