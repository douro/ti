package com.ti.cms.web.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ti.cms.web.system.domain.UserRole;
import org.apache.ibatis.annotations.Param;

/**
 * 用户角色Mapper
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

    /**
     * 根据用户Id删除该用户的角色关系
     *
     * @param userId 用户ID
     * @return boolean
     */
    Boolean deleteByUserId(@Param("userId") Long userId);

    /**
     * 根据角色Id删除该角色的用户关系
     *
     * @param roleId 角色ID
     * @return boolean
     */
    Boolean deleteByRoleId(@Param("roleId") Long roleId);
}
