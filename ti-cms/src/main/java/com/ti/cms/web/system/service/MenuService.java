package com.ti.cms.web.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ti.cms.web.system.domain.Menu;

import java.util.List;
import java.util.Map;

/**
 * 系统菜单服务层
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
public interface MenuService extends IService<Menu> {

    /**
     * 获取用户权限集合
     * 
     * @param username 用户名
     * @return List
     */
    List<Menu> findUserPermissions(String username);

    /**
     * 获取用户菜单集合
     *
     * @param username 用户名
     * @return List
     */
    List<Menu> findUserMenus(String username);

    /**
     * 获取菜单
     *
     * @param menu 菜单信息
     * @return Map
     */
    Map<String, Object> findMenus(Menu menu);

    /**
     * 获取菜单集合
     *
     * @param menu 菜单信息
     * @return List
     */
    List<Menu> findMenuList(Menu menu);

    /**
     * 创建菜单
     *
     * @param menu 菜单信息
     */
    void createMenu(Menu menu);

    /**
     * 修改菜单
     *
     * @param menu 菜单信息
     * @throws Exception            
     */
    void updateMenu(Menu menu) throws Exception;

    /**
     * 递归删除菜单/按钮
     *
     * @param menuIds menuIds
     * @throws Exception
     */
    void deleteMeuns(String[] menuIds) throws Exception;

}
