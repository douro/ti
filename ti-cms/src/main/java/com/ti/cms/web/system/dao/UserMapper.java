package com.ti.cms.web.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ti.cms.web.system.domain.User;
import org.apache.ibatis.annotations.Param;

/**
 * 用户表Mapper
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
public interface UserMapper extends BaseMapper<User> {

    /**
     * 获取用户列表
     *
     * @param page 分页对象
     * @param user 用户对象
     * @return 用户信息列表
     */
    IPage<User> findUserDetail(Page page, @Param("user") User user);

    /**
     * 获取单个用户详情
     *
     * @param username 用户名
     * @return 用户信息
     */
    User findDetail(String username);
}
