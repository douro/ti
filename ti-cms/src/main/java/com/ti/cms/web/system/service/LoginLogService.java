package com.ti.cms.web.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ti.cms.web.system.domain.LoginLog;

/**
 * 登录日志服务层
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
public interface LoginLogService extends IService<LoginLog> {

    /**
     * 保存登录日志
     *
     * @param loginLog 登录日志信息
     */
    void saveLoginLog(LoginLog loginLog);
}
