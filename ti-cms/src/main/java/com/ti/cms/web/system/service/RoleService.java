package com.ti.cms.web.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ti.cms.web.system.domain.Role;
import com.ti.core.domain.QueryRequest;

import java.util.List;

/**
 * 角色管理服务层
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
public interface RoleService extends IService<Role> {

    /**
     * 角色列表
     *
     * @param role 角色信息
     * @param request 分页对象
     * @return java.util.List<com.ti.cms.web.system.domain.Role>
     */
    IPage<Role> findRoles(Role role, QueryRequest request);

    /**
     * 角色集合
     *
     * @param userName 用户名
     * @return java.util.List<com.ti.cms.web.system.domain.Role>
     */
    List<Role> findUserRole(String userName);

    /**
     * 角色详情
     *
     * @param roleName 角色名称
     * @return Role
     */
    Role findByName(String roleName);

    /**
     * 创建角色
     *
     * @param role 角色信息
     */
    void createRole(Role role);

    /**
     * 删除角色
     *
     * @param roleIds 角色id集合
     * @throws Exception
     */
    void deleteRoles(String[] roleIds) throws Exception;

    /**
     * 修改角色
     *
     * @param role 角色信息
     * @throws Exception
     */
    void updateRole(Role role) throws Exception;
}
