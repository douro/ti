package com.ti.cms.web.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ti.core.base.BaseModel;
import lombok.Data;

import java.util.Date;

/**
 * 登录用户信息模型
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
@Data
@TableName("t_login_log")
public class LoginLog extends BaseModel<LoginLog> {

    /**
     * 用户 ID
     */
    private String username;

    /**
     * 登录时间
     */
    private Date loginTime;

    /**
     * 登录地点
     */
    private String location;

    /**
     * IP地址
     */
    private String ip;
}
