package com.ti.cms.web.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ti.cms.web.system.domain.Dept;
import com.ti.core.domain.QueryRequest;

import java.util.List;
import java.util.Map;

/**
 * 部门管理服务层
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
public interface DeptService extends IService<Dept> {

    /**
     * 部门信息
     *
     * @param request 分页对象
     * @param dept 部门信息
     * @return Map<String, Object>
     */
    Map<String, Object> findDepts(QueryRequest request, Dept dept);

    /**
     * 部门信息列表
     *
     * @param request 分页对象
     * @param dept 部门信息
     * @return IPage<Dict>
     */
    List<Dept> findDepts(Dept dept, QueryRequest request);

    /**
     * 新增部门
     *
     * @param dept 部门信息
     */
    void createDept(Dept dept);

    /**
     * 修改部门
     *
     * @param dept 部门信息
     */
    void updateDept(Dept dept);

    /**
     * 删除部门
     *
     * @param deptIds 部门id集合
     */
    void deleteDepts(String[] deptIds);
}
