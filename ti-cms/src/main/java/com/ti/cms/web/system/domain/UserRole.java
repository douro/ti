package com.ti.cms.web.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ti.core.base.BaseModel;

import lombok.Data;

/**
 * 用户角色模型
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
@Data
@TableName("t_user_role")
public class UserRole extends BaseModel<UserRole> {
	
	private static final long serialVersionUID = -3166012934498268403L;

	private Long userId;

	private Long roleId;

}
