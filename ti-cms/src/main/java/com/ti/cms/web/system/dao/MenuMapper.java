package com.ti.cms.web.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ti.cms.web.system.domain.Menu;

import java.util.List;

/**
 * 菜单表Mapper
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
public interface MenuMapper extends BaseMapper<Menu> {

    /**
     * 查找当前用户权限
     *
     * @param userName 用户账号
     * @return 菜单集合
     */
    List<Menu> findUserPermissions(String userName);

    /**
     * 查找当前用户菜单
     *
     * @param userName 用户账号
     * @return 菜单集合
     */
    List<Menu> findUserMenus(String userName);

    /**
     * 查找当前菜单/按钮关联的用户 ID
     *
     * @param menuId
     * @return 用户 ID集合
     */
    List<String> findUserIdsByMenuId(String menuId);
}
