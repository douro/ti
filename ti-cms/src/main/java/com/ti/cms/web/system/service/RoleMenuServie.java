package com.ti.cms.web.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ti.cms.web.system.domain.RoleMenu;

import java.util.List;

/**
 * 角色菜单服务层
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
public interface RoleMenuServie extends IService<RoleMenu> {

    /**
     * 根据角色id删除角色菜单
     *
     * @param roleIds 角色id集合
     */
    void deleteRoleMenusByRoleId(String[] roleIds);

    /**
     * 根据菜单id删除角色菜单
     *
     * @param menuIds 菜单id集合
     */
    void deleteRoleMenusByMenuId(String[] menuIds);

    /**
     * 根据角色id获取角色菜单
     *
     * @param roleId 角色id
     * @return java.util.List<com.ti.cms.web.system.domain.RoleMenu>
     */
    List<RoleMenu> getRoleMenusByRoleId(String roleId);
}
