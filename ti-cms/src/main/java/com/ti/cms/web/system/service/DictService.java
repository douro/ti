package com.ti.cms.web.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ti.cms.web.system.domain.Dict;
import com.ti.core.domain.QueryRequest;

/**
 * 字典管理服务层
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
public interface DictService extends IService<Dict> {

    /**
     * 字典信息列表
     *
     * @param request 分页对象
     * @param dict 字典信息
     * @return IPage<Dict>
     */
    IPage<Dict> findDicts(QueryRequest request, Dict dict);

    /**
     * 保存字典信息
     *
     * @param dict 字典信息
     */
    void createDict(Dict dict);

    /**
     * 修改字典信息
     *
     * @param dict 字典信息
     */
    void updateDict(Dict dict);

    /**
     * 删除字典信息
     *
     * @param dictIds 字典id集合
     */
    void deleteDicts(String[] dictIds);

}
