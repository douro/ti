package com.ti.cms.web.job.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ti.cms.web.job.domain.JobLog;
import com.ti.core.domain.QueryRequest;

/**
 * 调度日志逻辑服务层
 *
 * @author Lord
 * @date 2019/8/23 15:16
 */
public interface JobLogService extends IService<JobLog> {

    /**
     * 获取调度日志列表
     *
     * @param request 检索条件
     * @param jobLog 调度日志信息
     * @return
     * @see com.baomidou.mybatisplus.core.metadata.IPage<com.ti.cms.web.job.domain.JobLog>
     */
    IPage<JobLog> findJobLogs(QueryRequest request, JobLog jobLog);

    /**
     * 新增调度日志
     *
     * @param log 调度日志信息
     */
    void saveJobLog(JobLog log);

    /**
     * 删除调度日志
     *
     * @param jobLogIds 主键集合
     */
    void deleteJobLogs(String[] jobLogIds);
}
