package com.ti.cms.web.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ti.cms.web.system.domain.Test;

import java.util.List;

/**
 * 导入导出测试服务层
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
public interface TestService extends IService<Test> {

    /**
     * 导入导出列表
     *
     * @return java.util.List<com.ti.cms.web.system.domain.Test>
     */
    List<Test> findTests();

    /**
     * 批量插入
     * @param list List<Test>
     */
    void batchInsert(List<Test> list);
}
