package com.ti.cms.web.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ti.cms.web.system.domain.UserRole;

import java.util.List;

/**
 * 用户角色服务层
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
public interface UserRoleService extends IService<UserRole> {

	/**
	 * 根据角色id删除用户角色
	 *
	 * @param roleIds 角色id集合
	 */
	void deleteUserRolesByRoleId(String[] roleIds);

	/**
	 * 根据用户id删除用户角色
	 *
	 * @param userIds 用户id集合
	 */
	void deleteUserRolesByUserId(String[] userIds);

	/**
	 * 根据角色id查询用户id集合
	 *
	 * @param roleIds 角色id集合
	 * @return java.util.List<java.lang.String>
	 */
	List<String> findUserIdsByRoleId(String[] roleIds);
}
