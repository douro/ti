package com.ti.cms.web.job.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ti.cms.web.job.dao.JobLogMapper;
import com.ti.cms.web.job.domain.JobLog;
import com.ti.cms.web.job.service.JobLogService;
import com.ti.core.domain.QueryRequest;
import com.ti.core.lang.TisConstant;
import com.ti.core.utils.SortUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * 调度日志逻辑实现层
 *
 * @author Lord
 * @date 2019/8/23 15:16
 */
@Slf4j
@Service("JobLogService")
public class JobLogServiceImpl extends ServiceImpl<JobLogMapper, JobLog> implements JobLogService {

    @Override
    public IPage<JobLog> findJobLogs(QueryRequest request, JobLog jobLog) {
        try {
            LambdaQueryWrapper<JobLog> queryWrapper = new LambdaQueryWrapper<>();

            if (StringUtils.isNotBlank(jobLog.getBeanName())) {
                queryWrapper.eq(JobLog::getBeanName, jobLog.getBeanName());
            }
            if (StringUtils.isNotBlank(jobLog.getMethodName())) {
                queryWrapper.eq(JobLog::getMethodName, jobLog.getMethodName());
            }
            if (StringUtils.isNotBlank(jobLog.getParams())) {
                queryWrapper.like(JobLog::getParams, jobLog.getParams());
            }
            if (StringUtils.isNotBlank(jobLog.getStatus())) {
                queryWrapper.eq(JobLog::getStatus, jobLog.getStatus());
            }
            if (StringUtils.isNotBlank(jobLog.getCreateTimeFrom())
                    && StringUtils.isNotBlank(jobLog.getCreateTimeTo())) {
                queryWrapper.ge(JobLog::getCreateTime, jobLog.getCreateTimeFrom()).le(JobLog::getCreateTime,
                        jobLog.getCreateTimeTo());
            }

            Page<JobLog> page = new Page<>(request.getPageNum(), request.getPageSize());
            SortUtils.handlePageSort(request, page, TisConstant.ORDER_DESC, true);
            return this.page(page, queryWrapper);

        } catch (Exception e) {
            log.error("获取调度日志信息失败", e);
            return null;
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveJobLog(JobLog log) {
        this.save(log);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteJobLogs(String[] jobLogIds) {
        List<String> list = Arrays.asList(jobLogIds);
        this.baseMapper.deleteBatchIds(list);
    }

}
