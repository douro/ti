package com.ti.cms.web.job.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ti.cms.web.job.domain.JobLog;

/**
 * 任务日志表Mapper
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
public interface JobLogMapper extends BaseMapper<JobLog> {

}
