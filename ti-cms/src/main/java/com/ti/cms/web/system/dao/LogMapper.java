package com.ti.cms.web.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ti.cms.web.system.domain.SysLog;

/**
 * 系统日志表Mapper
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
public interface LogMapper extends BaseMapper<SysLog> {

}
