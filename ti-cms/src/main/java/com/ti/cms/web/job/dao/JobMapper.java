package com.ti.cms.web.job.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ti.cms.web.job.domain.Job;

import java.util.List;

/**
 * 任务调度表Mapper
 *
 * @author Lord
 * @date 2019/8/22 10:34
 */
public interface JobMapper extends BaseMapper<Job> {

	/**
	 * 获取任务调度列表
	 *
	 * @return Long
	 */
	List<Job> queryList();
}
